/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.publiic;

import dal.CourseDAO;
import dal.RegistrationDAO;
import dal.UserDAO;
import dal.UserRoleDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Registration;
import model.User;
import util.EmailUtil;
import util.Password;
import util.Validation;
import util.model.Email;

@WebServlet(name = "DetailAndRegisterCourse", urlPatterns = {"/p/course/detail"})
public class DetailAndRegisterCourse extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (request.getSession().getAttribute("user") != null) {
                request.setAttribute("user", (User) request.getSession().getAttribute("user"));
                request.setAttribute("registration", new RegistrationDAO().getByCourseIDAndUserID(Integer.parseInt(request.getParameter("courseID")), ((User) request.getSession().getAttribute("user")).getUserID()));
            }
            request.setAttribute("course", new CourseDAO().getByID(Integer.parseInt(request.getParameter("courseID"))));
            request.setAttribute("active", "course");
            request.getRequestDispatcher("../../public/course/detailAndRegisterCourse.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(DetailAndRegisterCourse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int courseID = Integer.parseInt(request.getParameter("courseID"));

            String email = request.getParameter("email");
            String fullname = request.getParameter("fullname");
            String mobile = request.getParameter("mobile");
            String gender = request.getParameter("gender");
            String favorContact = request.getParameter("favorContact");

            User user;
            UserDAO userDAO = new UserDAO();
            if (request.getSession().getAttribute("user") != null) {
                user = (User) request.getSession().getAttribute("user");
            } else {
                user = new User();
            }
            user.setEmail(email);
            user.setFullname(fullname);
            user.setMobile(mobile);
            user.setGender(gender);
            user.setFavorContact(favorContact);

            /* Với user chưa có tài khoản */
            if (request.getSession().getAttribute("user") == null) {
                //Kiểm tra email và trả lỗi nếu đã có user sử dụng email này
                if (userDAO.existsWithEmail(email)) {
                    request.setAttribute("errorMsg", "Email existed!");
                    request.setAttribute("user", user);
                    doGet(request, response);
                    return;
                }

                //Kiểm tra số điện thoại có hợp lệ không
                for (String mobileSplit : user.getMobileSplits()) {
                    if (!Validation.isPhoneValid(mobileSplit)) {
                        request.setAttribute("errorMsg", "'Mobile number: " + mobileSplit + "' is not valid!");
                        request.setAttribute("user", user);
                        doGet(request, response);
                        return;
                    }
                }

                /*Xử lý thêm mới user, thêm registration, gửi thông tin đăng nhập qua mail*/
                String password = Password.genPassword();
                user.setPassword(Password.getMD5Encoding(password));
                user.setRole(new UserRoleDAO().getByID(2));
                userDAO.insert(user);

                RegistrationDAO registrationDAO = new RegistrationDAO();
                registrationDAO.insert(new Registration(0, userDAO.getUserByEmail(email), new CourseDAO().getByID(courseID)));

                //Send email
                Email emailSend = EmailUtil.getRegistrationCourseEmail();
                emailSend.setTo(email);
                emailSend.setContent(
                        String.format(emailSend.getContent(),
                                fullname, //Name
                                email,
                                password, //Password
                                request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf(request.getContextPath())).concat(request.getContextPath()))); //Link login
                EmailUtil.sendEmail(emailSend);

                request.setAttribute("successMsg", "Register successfully! Please check your email for more information!");
                request.setAttribute("user", user);
                doGet(request, response);
                return;
            }

            /* Với user đã có tài khoản */
            userDAO.update(user);   //Update lại thông tin user
            request.getSession().setAttribute("user", user);    //Update lại thông tin user
            RegistrationDAO registrationDAO = new RegistrationDAO();
            registrationDAO.insert(new Registration(0, user, new CourseDAO().getByID(courseID)));
            request.setAttribute("successMsg", "Register course successfully!");
            doGet(request, response);
        } catch (Exception ex) {
            Logger.getLogger(DetailAndRegisterCourse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
