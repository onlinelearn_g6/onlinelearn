/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.publiic;

import dal.CategoryDAO;
import dal.CourseDAO;
import dal.DimensionDAO;
import dal.FeatureDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.model.Page;

@WebServlet(name = "ListCourse", urlPatterns = {"/p/courses"})
public class ListCourse extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String search = request.getAttribute("search").toString();
            Page page = (Page) request.getAttribute("page");
            CourseDAO courseDAO = new CourseDAO();

            //Get all course with condition
            if (request.getParameter("dimension") != null && !request.getParameter("dimension").isEmpty()) {
                int dimension = Integer.parseInt(request.getParameter("dimension"));
                request.setAttribute("dimension", dimension);
                request.setAttribute("courses", courseDAO.getAllByAttribute("dimension", dimension, search, page));
                request.setAttribute("page", page.calculate(courseDAO.countAllByAttribute("dimension", dimension, search)));
            } else if (request.getParameter("category") != null && !request.getParameter("category").isEmpty()) {
                int category = Integer.parseInt(request.getParameter("category"));
                request.setAttribute("category", category);
                request.setAttribute("courses", courseDAO.getAllByAttribute("category", category, search, page));
                request.setAttribute("page", page.calculate(courseDAO.countAllByAttribute("category", category, search)));
            } else if (request.getParameter("feature") != null && !request.getParameter("feature").isEmpty()) {
                int feature = Integer.parseInt(request.getParameter("feature"));
                request.setAttribute("feature", feature);
                request.setAttribute("courses", courseDAO.getAllByAttribute("feature", feature, search, page));
                request.setAttribute("page", page.calculate(courseDAO.countAllByAttribute("feature", feature, search)));
            } else {
                request.setAttribute("courses", courseDAO.getAll(search, page));
                request.setAttribute("page", page.calculate(courseDAO.countAll(search)));
            }

            //Get all dimensions
            DimensionDAO dimensionDAO = new DimensionDAO();
            request.setAttribute("dimensions", dimensionDAO.getAll("", new Page(1, 100)));

            //Get all categorys
            CategoryDAO categoryDAO = new CategoryDAO();
            request.setAttribute("categorys", categoryDAO.getAll("", new Page(1, 100)));

            //Get all features
            FeatureDAO featureDAO = new FeatureDAO();
            request.setAttribute("features", featureDAO.getAll("", new Page(1, 100)));

            request.setAttribute("search", search);
            request.setAttribute("active", "course");
            request.setAttribute("url", "/p/courses");
            request.getRequestDispatcher("../public/course/courses.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ListCourse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
