/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common.forgotpassword;

import dal.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;
import util.EmailUtil;
import util.Token;
import util.model.Email;

@WebServlet(name = "Fpwd", urlPatterns = {"/fpwd"})
public class Fpwd extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().invalidate();
        request.getRequestDispatcher("common/forgotpassword/fpwd.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String email = request.getParameter("email");
            User user = new UserDAO().getUserByEmail(email);

            request.setAttribute("email", email);
            if (user == null) {
                request.setAttribute("fpwdError", "Invalid email. Please check your email again!");
                doGet(request, response);
                return;
            }

            /* Set token and expire time */
            //Gen token
            String token = Token.genToken();
            //Get expire token time
            Date expireTokenTime = Token.getTokenExpire(1);
            user.setPrivateToken(token);
            user.setTokenValidTo(expireTokenTime);
            new UserDAO().update(user);

            //Send email
            Email emailSend = EmailUtil.getResetPassEmail();
            emailSend.setTo(email);
            emailSend.setContent(
                    String.format(emailSend.getContent(),
                            user.getFullname(), //Name
                            request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf(request.getContextPath())).concat(request.getContextPath()).concat("/resetPwd?token=").concat(token), //Link reset password
                            new SimpleDateFormat("HH:mm:ss dd/MM/yyyy").format(expireTokenTime))
            );  //Expire date
            EmailUtil.sendEmail(emailSend);

            request.getRequestDispatcher("common/forgotpassword/fpwdNotification.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Fpwd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
