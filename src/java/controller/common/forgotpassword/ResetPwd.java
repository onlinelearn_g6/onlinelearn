/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common.forgotpassword;

import dal.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;
import util.Password;
import util.Validation;

@WebServlet(name = "ResetPwd", urlPatterns = {"/resetPwd"})
public class ResetPwd extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            //User error when change pass (doPost)
            if (request.getAttribute("email") != null) {
                request.getRequestDispatcher("common/forgotpassword/resetPwd.jsp").forward(request, response);
                return;
            }

            String token = request.getParameter("token");
            if (token == null) {
                request.getSession().invalidate();
                request.getRequestDispatcher("error/error404.jsp").forward(request, response);
                return;
            }

            User user = new UserDAO().getUserByToken(token);
            if (user == null || (new Date().getTime()) > user.getTokenValidTo().getTime()) {
                request.getSession().invalidate();
                request.getRequestDispatcher("error/error404.jsp").forward(request, response);
                return;
            }

            request.setAttribute("email", user.getEmail());
            request.getRequestDispatcher("common/forgotpassword/resetPwd.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ResetPwd.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String email = request.getParameter("email");
            String newpassword = request.getParameter("newpassword");
            String renewpassword = request.getParameter("renewpassword");

            //Check new password and renew password are the same
            if (!newpassword.equals(renewpassword)) {
                request.setAttribute("resetPwdError", "'New Password' and 'Re-enter New Password' are not the same!");
                request.setAttribute("email", email);
                request.setAttribute("newpassword", newpassword);
                request.setAttribute("renewpassword", renewpassword);
                doGet(request, response);
                return;
            }

            //Check strong password
            if (!Validation.isStrongPassword(newpassword)) {
                request.setAttribute("resetPwdError", "New password is not strong enough! Try again!");
                request.setAttribute("email", email);
                request.setAttribute("newpassword", newpassword);
                request.setAttribute("renewpassword", renewpassword);
                doGet(request, response);
                return;
            }

            UserDAO userDAO = new UserDAO();
            User user = userDAO.getUserByEmail(email);

            //Update user information
            user.setPassword(Password.getMD5Encoding(newpassword));
            user.setTokenValidTo(null);
            user.setPrivateToken(null);
            userDAO.update(user);

            response.sendRedirect("login");
        } catch (Exception ex) {
            Logger.getLogger(ResetPwd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
