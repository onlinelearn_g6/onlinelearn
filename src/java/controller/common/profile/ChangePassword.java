/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common.profile;

import dal.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;
import util.Password;
import util.Validation;

@WebServlet(name = "ChangePassword", urlPatterns = {"/profile/chgpwd"})
public class ChangePassword extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("../common/profile/changepass.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String password = request.getParameter("password");
        String newpassword = request.getParameter("newpassword");
        String renewpassword = request.getParameter("renewpassword");

        //Check new password and renew password are the same
        if (!newpassword.equals(renewpassword)) {
            request.setAttribute("chgpwdError", "'New Password' and 'Re-enter New Password' are not the same!");
            request.setAttribute("password", password);
            request.setAttribute("newpassword", newpassword);
            request.setAttribute("renewpassword", renewpassword);
            doGet(request, response);
            return;
        }

        User user = (User) request.getSession().getAttribute("user");

        //Check current password is correct
        if (!user.getPassword().equals(Password.getMD5Encoding(password))) {
            request.setAttribute("chgpwdError", "'Current Password' is incorrect!");
            request.setAttribute("password", password);
            request.setAttribute("newpassword", newpassword);
            request.setAttribute("renewpassword", renewpassword);
            doGet(request, response);
            return;
        }

        //Check current password and new password are the same
        if (user.getPassword().equals(Password.getMD5Encoding(newpassword))) {
            request.setAttribute("chgpwdError", "'Current Password' and 'New Password' are the same!");
            request.setAttribute("password", password);
            request.setAttribute("newpassword", newpassword);
            request.setAttribute("renewpassword", renewpassword);
            doGet(request, response);
            return;
        }

        //Check strong password
        if (!Validation.isStrongPassword(newpassword)) {
            request.setAttribute("chgpwdError", "New password is not strong enough! Try again!");
            request.setAttribute("password", password);
            request.setAttribute("newpassword", newpassword);
            request.setAttribute("renewpassword", renewpassword);
            doGet(request, response);
            return;
        }

        //Update session user password
        user.setPassword(Password.getMD5Encoding(newpassword));

        UserDAO userDAO = new UserDAO();
        try {
            userDAO.update(user);
        } catch (Exception ex) {
            Logger.getLogger(ChangePassword.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setAttribute("chgpwdMsg", "Change password successfully!<br/>New password was updated!");
        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
