/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common.profile;

import dal.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;
import util.Validation;

@WebServlet(name = "Profile", urlPatterns = {"/profile"})
public class Profile extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("common/profile/profile.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String fullname = request.getParameter("fullname");
            String gender = request.getParameter("gender");
            String mobile = request.getParameter("mobile");
            String address = request.getParameter("address");

            User user = (User) request.getSession().getAttribute("user");
            user.setFullname(fullname);
            user.setGender(gender);
            user.setMobile(mobile);
            user.setAddress(address);

            //Kiểm tra số điện thoại có hợp lệ không
            for (String mobileSplit : user.getMobileSplits()) {
                if (!Validation.isPhoneValid(mobileSplit)) {
                    request.setAttribute("updateError", "'Mobile number: " + mobileSplit + "' is not valid!");
                    request.setAttribute("user", user);
                    doGet(request, response);
                    return;
                }
            }

            request.getSession().setAttribute("user", user);
            new UserDAO().update(user);

            request.setAttribute("user", user);
            request.setAttribute("updateMsg", "Update successfully!");
            doGet(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Profile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
