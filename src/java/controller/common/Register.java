/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.common;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/register"})
public class Register extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        RoleDAO roleDAO = new RoleDAO();
//        List<Role> roles = roleDAO.getGuestRegisterRole();
//        request.setAttribute("roles", roles);
        request.getRequestDispatcher("common/register.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        User user = new User();
//        String email = request.getParameter("email");
//        String password = request.getParameter("password");
//        String passwordcf = request.getParameter("passwordcf");
//        String username = request.getParameter("username");
//        String address = request.getParameter("address");
//        String firstname = request.getParameter("firstname");
//        String lastname = request.getParameter("lastname");
//        String phone = request.getParameter("phone");
//        user.setEmail(email);
//        user.setPassword(password);
//        user.setUsername(username);
//        user.setAddress(address);
//        user.setFirstname(firstname);
//        user.setLastname(lastname);
//        user.setPhone(phone);
//
//        UserDAO userDAO = new UserDAO();
//        if (userDAO.getUserByEmail(email)) {
//            request.setAttribute("errorMsg", "Email existed!");
//            request.setAttribute("user", user);
//            request.setAttribute("passwordcf", passwordcf);
//            doGet(request, response);
//            return;
//        }
//        if (userDAO.getUserByUsername(username)) {
//            request.setAttribute("errorMsg", "Username existed!");
//            request.setAttribute("user", user);
//            request.setAttribute("passwordcf", passwordcf);
//            doGet(request, response);
//            return;
//        }
//        if (!password.equals(passwordcf)) {
//            request.setAttribute("errorMsg", "'Password' and 'Confirm password' are not the same!");
//            request.setAttribute("user", user);
//            request.setAttribute("passwordcf", passwordcf);
//            doGet(request, response);
//            return;
//        }
//        if (!Validation.isStrongPassword(password)) {
//            request.setAttribute("errorMsg", "Your password is not strong enough! Try again!");
//            request.setAttribute("user", user);
//            request.setAttribute("passwordcf", passwordcf);
//            doGet(request, response);
//            return;
//        }
//        if (!Validation.isPhoneValid(phone)) {
//            request.setAttribute("errorMsg", "'Phone number' is not valid!");
//            request.setAttribute("user", user);
//            request.setAttribute("passwordcf", passwordcf);
//            doGet(request, response);
//            return;
//        }
//        String roleID = request.getParameter("roleID");
//        RoleDAO roleDAO = new RoleDAO();
//        Role role;
//        role = roleDAO.getRoleByID(Integer.parseInt(roleID));
//        user.setRole(role);
//        user.setIsApproved(false);
//        user.setStatus(true);
//        user.setPassword(MD5Encoding.getMD5(password));
//        userDAO.insert(user);
        response.sendRedirect("login");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
