/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin.user;

import dal.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.model.Page;

@WebServlet(name = "ListUser", urlPatterns = {"/ad/users"})
public class ListUser extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String search = request.getAttribute("search").toString();
            Page page = (Page) request.getAttribute("page");

            UserDAO userDAO = new UserDAO();
            request.setAttribute("users", userDAO.getAll(search, page));
            request.setAttribute("search", search);
            request.setAttribute("page", page.calculate(userDAO.countAll(search)));
            request.setAttribute("active", "user");
            request.setAttribute("url", "/ad/users");
            request.getRequestDispatcher("../admin/user/users.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ListUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
