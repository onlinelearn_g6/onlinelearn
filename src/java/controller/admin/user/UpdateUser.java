/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin.user;

import dal.UserDAO;
import dal.UserRoleDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

@WebServlet(name = "UpdateUser", urlPatterns = {"/ad/updateUser"})
public class UpdateUser extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            request.setAttribute("user", new UserDAO().getByID(Integer.parseInt(request.getParameter("userID"))));
            request.setAttribute("userRoles", new UserRoleDAO().getAll());
            request.setAttribute("active", "user");
            request.getRequestDispatcher("../admin/user/updateUser.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(UpdateUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int roleID = Integer.parseInt(request.getParameter("roleID"));
        boolean status = (request.getParameter("status") != null);
        String favorContact = request.getParameter("favorContact");

        try {
            UserDAO userDAO = new UserDAO();
            User user = userDAO.getByID(Integer.parseInt(request.getParameter("userID")));
            user.setRole(new UserRoleDAO().getByID(roleID));
            user.setStatus(status);
            user.setFavorContact(favorContact);
            userDAO.update(user);

            request.setAttribute("updateMsg", "Update user information successfully!");
            request.setAttribute("userID", user.getUserID());
            doGet(request, response);
        } catch (Exception ex) {
            Logger.getLogger(UpdateUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
