/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin.user;

import dal.UserDAO;
import dal.UserRoleDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;
import util.EmailUtil;
import util.Password;
import util.Validation;
import util.model.Email;

@WebServlet(name = "CreateUser", urlPatterns = {"/ad/createUser"})
public class CreateUser extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            request.setAttribute("active", "user");
            request.setAttribute("userRoles", new UserRoleDAO().getAll());
            request.getRequestDispatcher("../admin/user/createUser.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String email = request.getParameter("email");
            String fullname = request.getParameter("fullname");
            String mobile = request.getParameter("mobile");
            String gender = request.getParameter("gender");
            String address = request.getParameter("address");
            int roleID = Integer.parseInt(request.getParameter("roleID"));
            String favorContact = request.getParameter("favorContact");

            User user = new User(0, email, null, fullname, null, gender, mobile, address, true, null, null, new UserRoleDAO().getByID(roleID), favorContact);
            UserDAO userDAO = new UserDAO();

            if (userDAO.existsWithEmail(email)) {
                request.setAttribute("errorMsg", "Email existed!");
                request.setAttribute("user", user);
                doGet(request, response);
                return;
            }

            for (String mobileSplit : user.getMobileSplits()) {
                if (!Validation.isPhoneValid(mobileSplit)) {
                    request.setAttribute("errorMsg", "'Mobile number: " + mobileSplit + "' is not valid!");
                    request.setAttribute("user", user);
                    doGet(request, response);
                    return;
                }
            }

            String password = Password.genPassword();
            user.setPassword(Password.getMD5Encoding(password));

            userDAO.insert(user);

            //Send email
            Email emailSend = EmailUtil.getWelcomeEmail();
            emailSend.setTo(email);
            emailSend.setContent(
                    String.format(emailSend.getContent(),
                            user.getFullname(), //Name
                            password, //Password
                            request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf(request.getContextPath())).concat(request.getContextPath()))); //Link login
            EmailUtil.sendEmail(emailSend);

            request.setAttribute("createNewMsg", "Create new user successfully!");
            request.getRequestDispatcher("users").include(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
