/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin.setting;

import dal.SettingDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Setting;

@WebServlet(name = "CreateSetting", urlPatterns = {"/ad/createSetting"})
public class CreateSetting extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("active", "setting");
        request.getRequestDispatcher("../admin/setting/createSetting.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String name = request.getParameter("name");
            String mappedValue = request.getParameter("mappedValue");
            String type = request.getParameter("type");
            int order = Integer.parseInt(request.getParameter("order"));
            
            Setting setting = new Setting(0, name, mappedValue, type, order, true);
            new SettingDAO().insert(setting);
            request.setAttribute("createNewMsg", "Create new setting successfully!");
            request.getRequestDispatcher("settings").include(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CreateSetting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
