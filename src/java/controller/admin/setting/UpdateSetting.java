/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin.setting;

import dal.SettingDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Setting;

@WebServlet(name = "UpdateSetting", urlPatterns = {"/ad/updateSetting"})
public class UpdateSetting extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            request.setAttribute("setting", new SettingDAO().getByID(Integer.parseInt(request.getParameter("settingID"))));
            request.setAttribute("active", "setting");
            request.getRequestDispatcher("../admin/setting/updateSetting.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(UpdateSetting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int settingID = Integer.parseInt(request.getParameter("settingID"));
            String name = request.getParameter("name");
            String mappedValue = request.getParameter("mappedValue");
            String type = request.getParameter("type");
            int order = Integer.parseInt(request.getParameter("order"));
            boolean status = (request.getParameter("status") != null);
            
            Setting setting = new Setting(settingID, name, mappedValue, type, order, status);
            new SettingDAO().update(setting);
            
            request.setAttribute("updateMsg", "Update user information successfully!");
            request.setAttribute("settingID", setting.getSettingID());
            doGet(request, response);
        } catch (Exception ex) {
            Logger.getLogger(UpdateSetting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
