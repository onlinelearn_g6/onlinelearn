/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin.setting;

import dal.SettingDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.model.Page;

@WebServlet(name = "ListSetting", urlPatterns = {"/ad/settings"})
public class ListSetting extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String search = request.getAttribute("search").toString();
            Page page = (Page) request.getAttribute("page");

            SettingDAO settingDAO = new SettingDAO();
            request.setAttribute("settings", settingDAO.getAll(search, page));
            request.setAttribute("search", search);
            request.setAttribute("page", page.calculate(settingDAO.countAll(search)));
            request.setAttribute("active", "setting");
            request.setAttribute("url", "/ad/settings");
            request.getRequestDispatcher("../admin/setting/settings.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ListSetting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
