/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.customer;

import dal.CourseDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

@WebServlet(name = "RegistrationCourse", urlPatterns = {"/c/course/registration"})
public class RegistrationCourse extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            CourseDAO courseDAO = new CourseDAO();
            request.setAttribute("courses", courseDAO.getRegistrationCourses((User) request.getSession().getAttribute("user")));
            request.setAttribute("active", "registration");
//            request.setAttribute("url", "/c/course/registration");
            request.getRequestDispatcher("../../customer/course/registrationCourse.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(RegistrationCourse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
