package dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import util.model.Page;

public abstract class DBContext<T> {

    static Connection connection;

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        if (connection != null) {
            return connection;
        }

        String url = "jdbc:sqlserver://" + DBConfig.SERVER_NAME + ":" + DBConfig.PORT_NUMBER + ";databaseName=" + DBConfig.DB_NAME;
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        connection = DriverManager.getConnection(url, DBConfig.USER, DBConfig.PASSWORD);
        return connection;
    }
    
    public abstract T getByID(int id) throws Exception;

    public abstract List<T> getAll(String search, Page page) throws Exception;

    public abstract int countAll(String search) throws Exception;

    public abstract void insert(T t) throws Exception;

    public abstract void update(T t) throws Exception;

    public abstract void delete(T t) throws Exception;
}
