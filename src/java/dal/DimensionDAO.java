/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Dimension;
import util.model.Page;

public class DimensionDAO extends DBContext<Dimension>{

    @Override
    public Dimension getByID(int id) throws Exception {
        String sql = """
                    SELECT [DimensionID]
                          ,[Name]
                          ,[Type]
                          ,[Description]
                      FROM [dbo].[Dimension]
                    WHERE DimensionID = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Dimension dimension = new Dimension();
            dimension.setDimensionID(resultSet.getInt(1));
            dimension.setName(resultSet.getNString(2));
            dimension.setType(resultSet.getNString(3));
            dimension.setDescription(resultSet.getNString(4));
            return dimension;
        }
        return null;
    }

    @Override
    public List<Dimension> getAll(String search, Page page) throws Exception {
        List<Dimension> dimensions = new ArrayList<>();
        String sql = """
                    SELECT [DimensionID]
                        ,[Name]
                        ,[Type]
                        ,[Description]
                    FROM [dbo].[Dimension]
                    WHERE [Name] LIKE ? OR [Type] LIKE ? OR [Description] LIKE ?
                    ORDER BY [DimensionID] ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setNString(1, "%" + search + "%");
        preparedStatement.setNString(2, "%" + search + "%");
        preparedStatement.setNString(3, "%" + search + "%");
        preparedStatement.setInt(4, page.getItemsPerPage() * (page.getCurrentPage() - 1));
        preparedStatement.setInt(5, page.getItemsPerPage());

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Dimension dimension = new Dimension();
            dimension.setDimensionID(resultSet.getInt(1));
            dimension.setName(resultSet.getNString(2));
            dimension.setType(resultSet.getNString(3));
            dimension.setDescription(resultSet.getNString(4));
            dimensions.add(dimension);
        }
        return dimensions;
    }

    @Override
    public int countAll(String search) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void insert(Dimension t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(Dimension t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Dimension t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
