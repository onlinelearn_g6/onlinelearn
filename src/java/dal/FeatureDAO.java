/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Feature;
import util.model.Page;

public class FeatureDAO extends DBContext<Feature> {

    @Override
    public Feature getByID(int id) throws Exception {
        String sql = """
                    SELECT [FeatureID]
                          ,[Name]
                      FROM [dbo].[Feature]
                    WHERE FeatureID = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Feature feature = new Feature();
            feature.setFeatureID(resultSet.getInt(1));
            feature.setName(resultSet.getNString(2));
            return feature;
        }
        return null;
    }
    
    public List<Feature> getByCourseID(int courseID) throws Exception {
        List<Feature> features = new ArrayList<>();
        String sql = """
                    SELECT F.FeatureID, F.[Name]
                    FROM Feature F
                    INNER JOIN CourseFeature CF ON F.FeatureID = CF.FeatureID AND CourseID = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, courseID);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Feature feature = new Feature();
            feature.setFeatureID(resultSet.getInt(1));
            feature.setName(resultSet.getNString(2));
            features.add(feature);
        }
        return features;
    }

    @Override
    public List<Feature> getAll(String search, Page page) throws Exception {
        List<Feature> features = new ArrayList<>();
        String sql = """
                    SELECT [FeatureID]
                        ,[Name]
                    FROM [dbo].[Feature]
                    WHERE [Name] LIKE ?
                    ORDER BY [FeatureID] ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setNString(1, "%" + search + "%");
        preparedStatement.setInt(2, page.getItemsPerPage() * (page.getCurrentPage() - 1));
        preparedStatement.setInt(3, page.getItemsPerPage());

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Feature feature = new Feature();
            feature.setFeatureID(resultSet.getInt(1));
            feature.setName(resultSet.getNString(2));
            features.add(feature);
        }
        return features;
    }

    @Override
    public int countAll(String search) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void insert(Feature t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(Feature t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Feature t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
