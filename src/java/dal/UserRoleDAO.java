/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.util.ArrayList;
import java.util.List;
import model.UserRole;
import util.model.Page;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserRoleDAO extends DBContext<UserRole> {

    @Override
    public UserRole getByID(int id) throws Exception {
        String sql = """
                         SELECT [UserRoleID]
                               ,[Name]
                           FROM [dbo].[UserRole] where UserRoleID = ?""";
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        
        while (resultSet.next()) {
            UserRole role = new UserRole();
            role.setId(resultSet.getInt(1));
            role.setName(resultSet.getString(2));
            return role;
        }

        return null;
    }
    
    public List getAll() throws Exception {
        List<UserRole> list = new ArrayList<>();
        String sql = """
                        SELECT [UserRoleID]
                            ,[Name]
                        FROM [dbo].[UserRole]""";
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            UserRole role = new UserRole();
            role.setId(resultSet.getInt(1));
            role.setName(resultSet.getString(2));
            list.add(role);
        }
        return list;
    }

    @Override
    public List getAll(String search, Page page) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int countAll(String search) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void insert(UserRole t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(UserRole t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(UserRole t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
