/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import model.PricePackage;
import util.model.Page;

public class PricePackageDAO extends DBContext<PricePackage> {

    @Override
    public PricePackage getByID(int id) throws Exception {
        String sql = """
                    SELECT [PricePackageID]
                          ,[Name]
                          ,[Duration]
                          ,[OriginalPrice]
                          ,[SalePrice]
                          ,[Description]
                          ,[Status]
                      FROM [dbo].[PricePackage]
                    WHERE PricePackageID = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            PricePackage pricePackage = new PricePackage();
            pricePackage.setPricePackageID(resultSet.getInt(1));
            pricePackage.setName(resultSet.getNString(2));
            pricePackage.setDuration(resultSet.getInt(3));
            pricePackage.setOriginalPrice(resultSet.getFloat(4));
            pricePackage.setSalePrice(resultSet.getFloat(5));
            pricePackage.setDescription(resultSet.getNString(6));
            pricePackage.setStatus(resultSet.getBoolean(7));
            return pricePackage;
        }
        return null;
    }

    @Override
    public List<PricePackage> getAll(String search, Page page) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int countAll(String search) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void insert(PricePackage t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(PricePackage t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(PricePackage t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
