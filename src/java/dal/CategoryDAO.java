/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.util.List;
import model.Category;
import util.model.Page;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CategoryDAO extends DBContext<Category> {

    @Override
    public Category getByID(int id) throws Exception {
        String sql = """
                    SELECT [CategoryID]
                          ,[Name]
                      FROM [dbo].[Category]
                    WHERE CategoryID = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Category category = new Category();
            category.setCategoryID(resultSet.getInt(1));
            category.setName(resultSet.getNString(2));
            return category;
        }
        return null;
    }

    @Override
    public List<Category> getAll(String search, Page page) throws Exception {
        List<Category> categorys = new ArrayList<>();
        String sql = """
                    SELECT [CategoryID]
                          ,[Name]
                      FROM [dbo].[Category]
                    WHERE [Name] LIKE ?
                    ORDER BY [CategoryID] ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setNString(1, "%" + search + "%");
        preparedStatement.setInt(2, page.getItemsPerPage() * (page.getCurrentPage() - 1));
        preparedStatement.setInt(3, page.getItemsPerPage());

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Category category = new Category();
            category.setCategoryID(resultSet.getInt(1));
            category.setName(resultSet.getNString(2));
            categorys.add(category);
        }
        return categorys;
    }

    @Override
    public int countAll(String search) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void insert(Category t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(Category t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Category t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
