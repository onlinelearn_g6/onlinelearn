/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.util.ArrayList;
import java.util.List;
import model.Setting;
import util.model.Page;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SettingDAO extends DBContext<Setting> {

    @Override
    public Setting getByID(int id) throws Exception {
        String sql = """
                    SELECT [SettingID]
                          ,[Name]
                          ,[Mapped Value]
                          ,[Type]
                          ,[Order]
                          ,[Status]
                      FROM [OnlineLearn].[dbo].[Setting]
                    WHERE [SettingID] = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Setting setting = new Setting();
            setting.setSettingID(resultSet.getInt(1));
            setting.setName(resultSet.getNString(2));
            setting.setMappedValue(resultSet.getNString(3));
            setting.setType(resultSet.getNString(4));
            setting.setOrder(resultSet.getInt(5));
            setting.setStatus(resultSet.getBoolean(6));
            
            return setting;
        }
        return null;
    }

    @Override
    public List<Setting> getAll(String search, Page page) throws Exception {
        List<Setting> settings = new ArrayList<>();
        String sql = """
                    SELECT [SettingID]
                          ,[Name]
                          ,[Mapped Value]
                          ,[Type]
                          ,[Order]
                          ,[Status]
                      FROM [OnlineLearn].[dbo].[Setting]
                    WHERE [Name] LIKE ? OR [Mapped Value] LIKE ? OR [Type] LIKE ?
                    ORDER BY [SettingID] DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY""";
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setNString(1, "%" + search + "%");
        preparedStatement.setNString(2, "%" + search + "%");
        preparedStatement.setNString(3, "%" + search + "%");
        preparedStatement.setInt(4, page.getItemsPerPage() * (page.getCurrentPage() - 1));
        preparedStatement.setInt(5, page.getItemsPerPage());

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Setting setting = new Setting();
            setting.setSettingID(resultSet.getInt(1));
            setting.setName(resultSet.getNString(2));
            setting.setMappedValue(resultSet.getNString(3));
            setting.setType(resultSet.getNString(4));
            setting.setOrder(resultSet.getInt(5));
            setting.setStatus(resultSet.getBoolean(6));
            settings.add(setting);
        }
        return settings;
    }

    @Override
    public int countAll(String search) throws Exception {
        String sql = """
                    SELECT COUNT(*)
                    FROM [OnlineLearn].[dbo].[Setting]
                    WHERE [Name] LIKE ? OR [Mapped Value] LIKE ? OR [Type] LIKE ?""";
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, "%" + search + "%");
        preparedStatement.setNString(2, "%" + search + "%");
        preparedStatement.setString(3, "%" + search + "%");
        
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }

    @Override
    public void insert(Setting t) throws Exception {
        String sql = """
                    INSERT INTO [dbo].[Setting]
                        ([Name]
                        ,[Mapped Value]
                        ,[Type]
                        ,[Order]
                        ,[Status])
                    VALUES (?,?,?,?,?)
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setNString(1, t.getName());
        preparedStatement.setNString(2, t.getMappedValue());
        preparedStatement.setNString(3, t.getType());
        preparedStatement.setInt(4, t.getOrder());
        preparedStatement.setBoolean(5, t.isStatus());
        
        preparedStatement.execute();
    }

    @Override
    public void update(Setting t) throws Exception {
        String sql = """
                    UPDATE [dbo].[Setting]
                        SET [Name] = ?
                        ,[Mapped Value] = ?
                        ,[Type] = ?
                        ,[Order] = ?
                        ,[Status] = ?
                    WHERE [settingID] = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setNString(1, t.getName());
        preparedStatement.setNString(2, t.getMappedValue());
        preparedStatement.setNString(3, t.getType());
        preparedStatement.setInt(4, t.getOrder());
        preparedStatement.setBoolean(5, t.isStatus());
        preparedStatement.setInt(6, t.getSettingID());
        
        preparedStatement.execute();
    }

    @Override
    public void delete(Setting t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
