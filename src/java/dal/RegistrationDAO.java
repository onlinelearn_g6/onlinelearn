/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.util.List;
import model.Registration;
import util.model.Page;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class RegistrationDAO extends DBContext<Registration> {

    @Override
    public Registration getByID(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Registration getByCourseIDAndUserID(int courseID, int userID) throws Exception {
        String sql
                = """
                        SELECT [RegistrationID]
                            ,[CourseID]
                            ,[UserID]
                        FROM [dbo].[Registration] WHERE CourseID = ? AND [UserID] = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, courseID);
        preparedStatement.setInt(2, userID);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            Registration registration = new Registration();
            registration.setRegistrationID(resultSet.getInt(1));
            registration.setCourse(new CourseDAO().getByID(resultSet.getInt(2)));
            registration.setUser(new UserDAO().getByID(resultSet.getInt(3)));
            return registration;
        }
        return null;
    }

    @Override
    public List<Registration> getAll(String search, Page page) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int countAll(String search) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void insert(Registration t) throws Exception {
        String sql = """
                    INSERT INTO [dbo].[Registration]
                               ([CourseID]
                               ,[UserID])
                    VALUES (?,?)
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, t.getCourse().getCourseID());
        preparedStatement.setInt(2, t.getUser().getUserID());

        preparedStatement.execute();
    }

    @Override
    public void update(Registration t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Registration t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
