/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Course;
import model.User;
import util.model.Page;

public class CourseDAO extends DBContext<Course> {

    @Override
    public Course getByID(int id) throws Exception {
        String sql = """
                    SELECT [CourseID]
                          ,[Name]
                          ,[BriefInformation]
                          ,[ThumbnailImage]
                          ,[Description]
                          ,[Status]
                          ,[isPublished]
                          ,[OwnerUserID]
                          ,[PricePackageID]
                          ,[DimensionID]
                          ,[CategoryID]
                          ,[CreatedAt]
                          ,[LastModifiedAt]
                      FROM [dbo].[Course]
                    WHERE CourseID = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Course course = new Course();
            course.setCourseID(resultSet.getInt(1));
            course.setName(resultSet.getNString(2));
            course.setBriefInformation(resultSet.getNString(3));
            course.setThumbnailImage(resultSet.getString(4));
            course.setDescription(resultSet.getNString(5));
            course.setStatus(resultSet.getBoolean(6));
            course.setIsPublished(resultSet.getBoolean(7));
            course.setOwnerUser(new UserDAO().getByID(resultSet.getInt(8)));
            course.setPricePackage(new PricePackageDAO().getByID(resultSet.getInt(9)));
            course.setDimension(new DimensionDAO().getByID(resultSet.getInt(10)));
            course.setCategory(new CategoryDAO().getByID(resultSet.getInt(11)));
            course.setCreateAt(resultSet.getDate(12));
            course.setLastModifiedAt(resultSet.getDate(13));
            course.setFeatures(new FeatureDAO().getByCourseID(course.getCourseID()));
            return course;
        }
        return null;
    }

    @Override
    public List<Course> getAll(String search, Page page) throws Exception {
        List<Course> courses = new ArrayList<>();
        String sql = """
                    SELECT [CourseID]
                          ,[Name]
                          ,[BriefInformation]
                          ,[ThumbnailImage]
                          ,[Description]
                          ,[Status]
                          ,[isPublished]
                          ,[OwnerUserID]
                          ,[PricePackageID]
                          ,[DimensionID]
                          ,[CategoryID]
                          ,[CreatedAt]
                          ,[LastModifiedAt]
                      FROM [dbo].[Course]
                    WHERE [Name] LIKE ? OR [BriefInformation] LIKE ? OR [Description] LIKE ?
                    ORDER BY [LastModifiedAt] DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setNString(1, "%" + search + "%");
        preparedStatement.setNString(2, "%" + search + "%");
        preparedStatement.setNString(3, "%" + search + "%");
        preparedStatement.setInt(4, page.getItemsPerPage() * (page.getCurrentPage() - 1));
        preparedStatement.setInt(5, page.getItemsPerPage());

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Course course = new Course();
            course.setCourseID(resultSet.getInt(1));
            course.setName(resultSet.getNString(2));
            course.setBriefInformation(resultSet.getNString(3));
            course.setThumbnailImage(resultSet.getString(4));
            course.setDescription(resultSet.getNString(5));
            course.setStatus(resultSet.getBoolean(6));
            course.setIsPublished(resultSet.getBoolean(7));
            course.setOwnerUser(new UserDAO().getByID(resultSet.getInt(8)));
            course.setPricePackage(new PricePackageDAO().getByID(resultSet.getInt(9)));
            course.setDimension(new DimensionDAO().getByID(resultSet.getInt(10)));
            course.setCategory(new CategoryDAO().getByID(resultSet.getInt(11)));
            course.setCreateAt(resultSet.getDate(12));
            course.setLastModifiedAt(resultSet.getDate(13));
            course.setFeatures(new FeatureDAO().getByCourseID(course.getCourseID()));
            courses.add(course);
        }
        return courses;
    }

    public List<Course> getRegistrationCourses(User user) throws Exception {
        List<Course> courses = new ArrayList<>();
        String sql = """
                    SELECT [CourseID]
                          ,[Name]
                          ,[BriefInformation]
                          ,[ThumbnailImage]
                          ,[Description]
                          ,[Status]
                          ,[isPublished]
                          ,[OwnerUserID]
                          ,[PricePackageID]
                          ,[DimensionID]
                          ,[CategoryID]
                          ,[CreatedAt]
                          ,[LastModifiedAt]
                      FROM [dbo].[Course]
                    WHERE CourseID IN (SELECT R.CourseID FROM Registration R WHERE R.UserID = ?)
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, user.getUserID());

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Course course = new Course();
            course.setCourseID(resultSet.getInt(1));
            course.setName(resultSet.getNString(2));
            course.setBriefInformation(resultSet.getNString(3));
            course.setThumbnailImage(resultSet.getString(4));
            course.setDescription(resultSet.getNString(5));
            course.setStatus(resultSet.getBoolean(6));
            course.setIsPublished(resultSet.getBoolean(7));
            course.setOwnerUser(new UserDAO().getByID(resultSet.getInt(8)));
            course.setPricePackage(new PricePackageDAO().getByID(resultSet.getInt(9)));
            course.setDimension(new DimensionDAO().getByID(resultSet.getInt(10)));
            course.setCategory(new CategoryDAO().getByID(resultSet.getInt(11)));
            course.setCreateAt(resultSet.getDate(12));
            course.setLastModifiedAt(resultSet.getDate(13));
            course.setFeatures(new FeatureDAO().getByCourseID(course.getCourseID()));
            courses.add(course);
        }
        return courses;
    }

    @Override
    public int countAll(String search) throws Exception {
        String sql = """
                    SELECT COUNT(*)
                    FROM [OnlineLearn].[dbo].[Course]
                    WHERE [Name] LIKE ? OR [BriefInformation] LIKE ? OR [Description] LIKE ?""";
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, "%" + search + "%");
        preparedStatement.setNString(2, "%" + search + "%");
        preparedStatement.setString(3, "%" + search + "%");

        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }

    @Override
    public void insert(Course t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void update(Course t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void delete(Course t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<Course> getAllByAttribute(String attribute, int attributeID, String search, Page page) throws Exception {
        List<Course> courses = new ArrayList<>();
        StringBuilder attributeCondition = new StringBuilder();

        switch (attribute) {
            case "dimension" ->
                attributeCondition.append("AND ").append("DimensionID = ?").append(" ");
            case "category" ->
                attributeCondition.append("AND ").append("CategoryID = ?").append(" ");
            case "feature" ->
                attributeCondition.append("AND ").append("CourseID IN (SELECT CourseID FROM CourseFeature CF WHERE CF.CourseID = CourseID AND FeatureID = ?)").append(" ");
        }

        String sql = """
                    SELECT [CourseID]
                          ,[Name]
                          ,[BriefInformation]
                          ,[ThumbnailImage]
                          ,[Description]
                          ,[Status]
                          ,[isPublished]
                          ,[OwnerUserID]
                          ,[PricePackageID]
                          ,[DimensionID]
                          ,[CategoryID]
                          ,[CreatedAt]
                          ,[LastModifiedAt]
                      FROM [dbo].[Course]
                    WHERE ([Name] LIKE ? OR [BriefInformation] LIKE ? OR [Description] LIKE ?)
                    """ + attributeCondition + """
                    ORDER BY [LastModifiedAt] DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY
                    """;

        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setNString(1, "%" + search + "%");
        preparedStatement.setNString(2, "%" + search + "%");
        preparedStatement.setNString(3, "%" + search + "%");
        preparedStatement.setInt(4, attributeID);
        preparedStatement.setInt(5, page.getItemsPerPage() * (page.getCurrentPage() - 1));
        preparedStatement.setInt(6, page.getItemsPerPage());

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Course course = new Course();
            course.setCourseID(resultSet.getInt(1));
            course.setName(resultSet.getNString(2));
            course.setBriefInformation(resultSet.getNString(3));
            course.setThumbnailImage(resultSet.getString(4));
            course.setDescription(resultSet.getNString(5));
            course.setStatus(resultSet.getBoolean(6));
            course.setIsPublished(resultSet.getBoolean(7));
            course.setOwnerUser(new UserDAO().getByID(resultSet.getInt(8)));
            course.setPricePackage(new PricePackageDAO().getByID(resultSet.getInt(9)));
            course.setDimension(new DimensionDAO().getByID(resultSet.getInt(10)));
            course.setCategory(new CategoryDAO().getByID(resultSet.getInt(11)));
            course.setCreateAt(resultSet.getDate(12));
            course.setLastModifiedAt(resultSet.getDate(13));
            course.setFeatures(new FeatureDAO().getByCourseID(course.getCourseID()));
            courses.add(course);
        }
        return courses;
    }

    public int countAllByAttribute(String attribute, int attributeID, String search) throws Exception {
        StringBuilder attributeCondition = new StringBuilder();

        switch (attribute) {
            case "dimension" ->
                attributeCondition.append("AND ").append("DimensionID = ?").append(" ");
            case "category" ->
                attributeCondition.append("AND ").append("CategoryID = ?").append(" ");
            case "feature" ->
                attributeCondition.append("AND ").append("CourseID IN (SELECT CourseID FROM CourseFeature CF WHERE CF.CourseID = CourseID AND FeatureID = ?)").append(" ");
        }

        String sql = """
                    SELECT COUNT(*)
                    FROM [OnlineLearn].[dbo].[Course]
                    WHERE ([Name] LIKE ? OR [BriefInformation] LIKE ? OR [Description] LIKE ?)
                    """ + attributeCondition;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, "%" + search + "%");
        preparedStatement.setNString(2, "%" + search + "%");
        preparedStatement.setString(3, "%" + search + "%");
        preparedStatement.setInt(4, attributeID);

        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }
}
