/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Timestamp;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import model.User;
import util.model.Page;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDAO extends DBContext<User> {

    public User getUserLogin(String email, String password) throws Exception {
        String sql
                = """
                        SELECT [UserID]
                            ,[Email]
                            ,[Password]
                            ,[FullName]
                            ,[Avatar]
                            ,[Gender]
                            ,[Mobile]
                            ,[Address]
                            ,[Status]
                            ,[PrivateToken]
                            ,[TokenValidTo]
                            ,[UserRoleID]
                            ,[FavorContact]
                        FROM [dbo].[User] WHERE Email = ? AND [Password] = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, email);
        preparedStatement.setString(2, password);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            User user = new User();
            user.setUserID(resultSet.getInt(1));
            user.setEmail(resultSet.getString(2));
            user.setPassword(resultSet.getString(3));
            user.setFullname(resultSet.getNString(4));
            user.setAvatar(resultSet.getBlob(5));
            user.setGender(resultSet.getString(6));
            user.setMobile(resultSet.getString(7));
            user.setAddress(resultSet.getString(8));
            user.setStatus(resultSet.getBoolean(9));
            user.setPrivateToken(resultSet.getString(10));
            user.setTokenValidTo(resultSet.getTimestamp(11));
            UserRoleDAO userRoleDAO = new UserRoleDAO();
            user.setRole(userRoleDAO.getByID(resultSet.getInt(12)));
            user.setFavorContact(resultSet.getString(13));
            return user;
        }
        return null;
    }

    public User getUserByEmail(String email) throws Exception {
        String sql
                = """
                        SELECT [UserID]
                            ,[Email]
                            ,[Password]
                            ,[FullName]
                            ,[Avatar]
                            ,[Gender]
                            ,[Mobile]
                            ,[Address]
                            ,[Status]
                            ,[PrivateToken]
                            ,[TokenValidTo]
                            ,[UserRoleID]
                            ,[FavorContact]
                        FROM [dbo].[User] WHERE Email = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, email);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            User user = new User();
            user.setUserID(resultSet.getInt(1));
            user.setEmail(resultSet.getString(2));
            user.setPassword(resultSet.getString(3));
            user.setFullname(resultSet.getNString(4));
            user.setAvatar(resultSet.getBlob(5));
            user.setGender(resultSet.getString(6));
            user.setMobile(resultSet.getString(7));
            user.setAddress(resultSet.getString(8));
            user.setStatus(resultSet.getBoolean(9));
            user.setPrivateToken(resultSet.getString(10));
            user.setTokenValidTo(resultSet.getTimestamp(11));
            UserRoleDAO userRoleDAO = new UserRoleDAO();
            user.setRole(userRoleDAO.getByID(resultSet.getInt(12)));
            user.setFavorContact(resultSet.getString(13));
            return user;
        }
        return null;
    }

    public User getUserByToken(String token) throws Exception {
        String sql
                = """
                        SELECT [UserID]
                            ,[Email]
                            ,[Password]
                            ,[FullName]
                            ,[Avatar]
                            ,[Gender]
                            ,[Mobile]
                            ,[Address]
                            ,[Status]
                            ,[PrivateToken]
                            ,[TokenValidTo]
                            ,[UserRoleID]
                            ,[FavorContact]
                        FROM [dbo].[User] WHERE PrivateToken = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, token);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            User user = new User();
            user.setUserID(resultSet.getInt(1));
            user.setEmail(resultSet.getString(2));
            user.setPassword(resultSet.getString(3));
            user.setFullname(resultSet.getNString(4));
            user.setAvatar(resultSet.getBlob(5));
            user.setGender(resultSet.getString(6));
            user.setMobile(resultSet.getString(7));
            user.setAddress(resultSet.getString(8));
            user.setStatus(resultSet.getBoolean(9));
            user.setPrivateToken(resultSet.getString(10));
            user.setTokenValidTo(resultSet.getTimestamp(11));
            UserRoleDAO userRoleDAO = new UserRoleDAO();
            user.setRole(userRoleDAO.getByID(resultSet.getInt(12)));
            user.setFavorContact(resultSet.getString(13));
            return user;
        }
        return null;
    }

    public boolean existsWithEmail(String email) throws Exception {
        String sql
                = """
                    SELECT [UserID]
                        ,[Email]
                        ,[Password]
                        ,[FullName]
                        ,[Avatar]
                        ,[Gender]
                        ,[Mobile]
                        ,[Address]
                        ,[Status]
                        ,[PrivateToken]
                        ,[TokenValidTo]
                        ,[UserRoleID]
                        ,[FavorContact]
                    FROM [dbo].[User] WHERE Email = ?
                """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, email);
        ResultSet resultSet = preparedStatement.executeQuery();

        return resultSet.next();
    }

    @Override
    public List<User> getAll(String search, Page page) throws Exception {
        List<User> users = new ArrayList<>();
        String sql = """
                    SELECT [UserID]
                          ,[Email]
                          ,[Password]
                          ,[FullName]
                          ,[Avatar]
                          ,[Gender]
                          ,[Mobile]
                          ,[Address]
                          ,[Status]
                          ,[PrivateToken]
                          ,[TokenValidTo]
                          ,[UserRoleID]
                          ,[FavorContact]
                      FROM [dbo].[User]
                    WHERE [Email] LIKE ? OR [FullName] LIKE ? OR [Mobile] LIKE ?
                        OR [Address] LIKE ?
                    ORDER BY [UserID] DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY""";
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, "%" + search + "%");
        preparedStatement.setNString(2, "%" + search + "%");
        preparedStatement.setString(3, "%" + search + "%");
        preparedStatement.setNString(4, "%" + search + "%");
        preparedStatement.setInt(5, page.getItemsPerPage() * (page.getCurrentPage() - 1));
        preparedStatement.setInt(6, page.getItemsPerPage());

        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            User user = new User();
            user.setUserID(resultSet.getInt(1));
            user.setEmail(resultSet.getString(2));
            user.setPassword(resultSet.getString(3));
            user.setFullname(resultSet.getNString(4));
            user.setAvatar(resultSet.getBlob(5));
            user.setGender(resultSet.getString(6));
            user.setMobile(resultSet.getString(7));
            user.setAddress(resultSet.getString(8));
            user.setStatus(resultSet.getBoolean(9));
            user.setPrivateToken(resultSet.getString(10));
            user.setTokenValidTo(resultSet.getTimestamp(11));
            UserRoleDAO userRoleDAO = new UserRoleDAO();
            user.setRole(userRoleDAO.getByID(resultSet.getInt(12)));
            user.setFavorContact(resultSet.getString(13));
            users.add(user);
        }
        return users;
    }

    @Override
    public int countAll(String search) throws Exception {
        String sql = """
                    SELECT COUNT(*)
                    FROM [dbo].[User]
                    WHERE [Email] LIKE ? OR [FullName] LIKE ? OR [Mobile] LIKE ?
                        OR [Address] LIKE ?""";
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, "%" + search + "%");
        preparedStatement.setNString(2, "%" + search + "%");
        preparedStatement.setString(3, "%" + search + "%");
        preparedStatement.setNString(4, "%" + search + "%");

        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }

    @Override
    public User getByID(int id) throws Exception {
        String sql = """
                    SELECT [UserID]
                          ,[Email]
                          ,[Password]
                          ,[FullName]
                          ,[Avatar]
                          ,[Gender]
                          ,[Mobile]
                          ,[Address]
                          ,[Status]
                          ,[PrivateToken]
                          ,[TokenValidTo]
                          ,[UserRoleID]
                          ,[FavorContact]
                      FROM [dbo].[User]
                    WHERE [UserID] =?""";
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            User user = new User();
            user.setUserID(resultSet.getInt(1));
            user.setEmail(resultSet.getString(2));
            user.setPassword(resultSet.getString(3));
            user.setFullname(resultSet.getNString(4));
            user.setAvatar(resultSet.getBlob(5));
            user.setGender(resultSet.getString(6));
            user.setMobile(resultSet.getString(7));
            user.setAddress(resultSet.getString(8));
            user.setStatus(resultSet.getBoolean(9));
            user.setPrivateToken(resultSet.getString(10));
            user.setTokenValidTo(resultSet.getTimestamp(11));
            UserRoleDAO userRoleDAO = new UserRoleDAO();
            user.setRole(userRoleDAO.getByID(resultSet.getInt(12)));
            user.setFavorContact(resultSet.getString(13));
            return user;
        }
        return null;
    }

    @Override
    public void insert(User t) throws Exception {
        String sql = """
                    INSERT INTO [dbo].[User]
                        ([Email]
                        ,[Password]
                        ,[FullName]
                        ,[Avatar]
                        ,[Gender]
                        ,[Mobile]
                        ,[Address]
                        ,[Status]
                        ,[PrivateToken]
                        ,[TokenValidTo]
                        ,[UserRoleID]
                        ,[FavorContact])
                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?)
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, t.getEmail());
        preparedStatement.setString(2, t.getPassword());
        preparedStatement.setNString(3, t.getFullname());
        preparedStatement.setBlob(4, t.getAvatar());
        preparedStatement.setString(5, t.getGender());
        preparedStatement.setString(6, t.getMobile());
        preparedStatement.setNString(7, t.getAddress());
        preparedStatement.setBoolean(8, t.isStatus());
        preparedStatement.setString(9, t.getPrivateToken());
        preparedStatement.setTimestamp(10, t.getTokenValidTo() == null ? null : new Timestamp(t.getTokenValidTo().getTime()));
        preparedStatement.setInt(11, t.getRole().getId());
        preparedStatement.setString(12, t.getFavorContact());

        preparedStatement.execute();
    }

    @Override
    public void update(User t) throws Exception {
        String sql = """
                    UPDATE [dbo].[User]
                        SET [Email] = ?
                        ,[Password] = ?
                        ,[FullName] = ?
                        ,[Avatar] = ?
                        ,[Gender] = ?
                        ,[Mobile] = ?
                        ,[Address] = ?
                        ,[Status] = ?
                        ,[PrivateToken] = ?
                        ,[TokenValidTo] = ?
                        ,[UserRoleID] = ?
                        ,[FavorContact] = ?
                    WHERE [UserID] = ?
                    """;
        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
        preparedStatement.setString(1, t.getEmail());
        preparedStatement.setString(2, t.getPassword());
        preparedStatement.setNString(3, t.getFullname());
        preparedStatement.setBlob(4, t.getAvatar());
        preparedStatement.setString(5, t.getGender());
        preparedStatement.setString(6, t.getMobile());
        preparedStatement.setNString(7, t.getAddress());
        preparedStatement.setBoolean(8, t.isStatus());
        preparedStatement.setString(9, t.getPrivateToken());
        preparedStatement.setTimestamp(10, t.getTokenValidTo() == null ? null : new Timestamp(t.getTokenValidTo().getTime()));
        preparedStatement.setInt(11, t.getRole().getId());
        preparedStatement.setString(12, t.getFavorContact());
        preparedStatement.setInt(13, t.getUserID());

        preparedStatement.execute();
    }

    @Override
    public void delete(User t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
