package util;

public class Validation {

    public static boolean isPhoneValid(String phone) {
        //Remove unneccessary character
        phone = phone.replaceAll("-", "");
        phone = phone.replaceAll("\\.", "");
        phone = phone.replaceAll("\\(", "");
        phone = phone.replaceAll("\\)", "");
        phone = phone.replaceAll("\\s", "");
        
        String regex = "^((\\+84)|0)(\\d{9})$";

        return phone.matches(regex);
    }
    
    public static boolean isStrongPassword(String password) {
        String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$";
        return password.matches(regex);
    }
}
