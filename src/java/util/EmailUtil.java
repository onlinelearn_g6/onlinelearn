package util;

import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import java.util.Properties;
import util.model.Email;

public class EmailUtil {

    public static void sendEmail(Email email) {
        final String username = "lamlvhe140728@fpt.edu.vn";
        //Google: Setting account => Security => 2-Step Verification => App passwords
        final String password = "vwfq raqn nxmz rpvo";
        String host = "smtp.gmail.com";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new jakarta.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email.getFrom()));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(email.getTo()));
            message.setSubject(email.getSubject());
            message.setContent(email.getContent(), "text/html");
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public static Email getResetPassEmail() {
        return new Email(
                "lamlvhe140728@fpt.edu.vn",
                "",
                "[Online Learn] Reset password",
                """
                <h1 style="font-weight: 700; color: #012970; font-family: 'Nunito, sans-serif'; text-align: center;">Online Learn</h1>
                <p style="font-weight: bold; text-align: center;">Reset password</p>
                <hr>
                <p>Hi %s,</p>
                <p>You are receiving this email because we received a password reset request for your account.</p>
                <a style="text-align: center;" href="%s">Reset your password</a>
                <p>This password reset link will expire at <strong>%s</strong>.</p>
                <p>If you did not request a password reset, no futhur action is required.</p>
                <p>Regards,</p>
                <p>Online Learn Team</p>
            """);
    }

    public static Email getWelcomeEmail() {
        return new Email(
                "lamlvhe140728@fpt.edu.vn",
                "",
                "[Online Learn] Welcome to Online Learn - Let's get started!",
                """
                <h1 style="font-weight: 700; color: #012970; font-family: 'Nunito, sans-serif'; text-align: center;">Online Learn</h1>
                <h3 style="font-weight: bold; text-align: center;">Welcome to Online Learn!!!</h3>
                <hr>
                <p>Hi %s,</p>
                <p>Thanks for trying Online Learn! Your account was created by admin recently.</p>
                <strong style="margin-left: 50px;">Your password: </strong><span style="background-color: yellow;">%s</span>
                <p>You probably already have a idea for how you're going to invest in your career with Online Learn.</p>
                <p>Let get access to videos in over 90%% of courses, Specializations, and Professional Certificates taught by top instructors from leading universities and companies.</p>
                <a style="padding-left: 150px;" href="%s">Login to Online Learn to explore more!</a>
                <p>Regards,</p>
                <p>Online Learn Team</p>
            """);
    }
    
    public static Email getRegistrationCourseEmail() {
        return new Email(
                "lamlvhe140728@fpt.edu.vn",
                "",
                "[Online Learn] Welcome to Online Learn - Let's get started with course!",
                """
                <h1 style="font-weight: 700; color: #012970; font-family: 'Nunito, sans-serif'; text-align: center;">Online Learn</h1>
                <h3 style="font-weight: bold; text-align: center;">Welcome to Online Learn!!!</h3>
                <hr>
                <p>Hi %s,</p>
                <p>Thanks for trying Online Learn!</p>
                <p>Your account recently registered a new course.</p>
                <p>A new account was created with your information.</p>
                <p><strong style="margin-left: 50px;">Your email: </strong><span style="background-color: yellow;">%s</span></p>
                <p><strong style="margin-left: 50px;">Your password: </strong><span style="background-color: yellow;">%s</span></p>
                <a style="padding-left: 150px;" href="%s">Login to Online Learn and start your course!</a>
                <p>Regards,</p>
                <p>Online Learn Team</p>
            """);
    }
}
