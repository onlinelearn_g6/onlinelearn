package util.model;

public class Page {

    private int itemsPerPage;
    private int previous;
    private int next;
    private int currentPage;
    private int totalPage;

    public Page() {
    }

    public Page(int currentPage, int itemsPerPage) {
        this.currentPage = currentPage;
        this.itemsPerPage = itemsPerPage;
    }

    public Page calculate(int countAll) {
        this.currentPage = (this.currentPage == 0 ? 1 : this.currentPage);
        this.totalPage = (countAll % this.itemsPerPage == 0 ? countAll / this.itemsPerPage : countAll / this.itemsPerPage + 1);
        this.previous = (this.currentPage > 1 ? this.currentPage - 1 : 1);
        this.next = (totalPage == 0 ? 1 : (this.currentPage == totalPage ? totalPage : this.currentPage + 1));
        return this;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public int getPrevious() {
        return previous;
    }

    public void setPrevious(int previous) {
        this.previous = previous;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotal(int totalPage) {
        this.totalPage = totalPage;
    }
}
