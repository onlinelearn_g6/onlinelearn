package util;

import java.time.Duration;
import java.util.Date;
import java.util.Random;

public class Token {

    public static String genToken() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int tokenLength = 20;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(tokenLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static Date getTokenExpire(long hours) {    //1 hour
        return Date.from(new Date().toInstant().plus(Duration.ofHours(1)));
    }
}
