/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;
import java.util.List;

public class Course {

    private int courseID;
    private String name;
    private String briefInformation;
    private String thumbnailImage;
    private String description;
    private boolean status;
    private boolean isPublished;
    private User ownerUser;
    private PricePackage pricePackage;
    private Dimension dimension;
    private Category category;
    private Date createAt;
    private Date lastModifiedAt;
    private List<Feature> features;

    public Course() {
    }

    public Course(int courseID, String name, String briefInformation, String thumbnailImage, String description, boolean status, boolean isPublished, User ownerUser, PricePackage pricePackage, Dimension dimension, Category category, Date createAt, Date lastModifiedAt, List<Feature> features) {
        this.courseID = courseID;
        this.name = name;
        this.briefInformation = briefInformation;
        this.thumbnailImage = thumbnailImage;
        this.description = description;
        this.status = status;
        this.isPublished = isPublished;
        this.ownerUser = ownerUser;
        this.pricePackage = pricePackage;
        this.dimension = dimension;
        this.category = category;
        this.createAt = createAt;
        this.lastModifiedAt = lastModifiedAt;
        this.features = features;
    }

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBriefInformation() {
        return briefInformation;
    }

    public void setBriefInformation(String briefInformation) {
        this.briefInformation = briefInformation;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isIsPublished() {
        return isPublished;
    }

    public void setIsPublished(boolean isPublished) {
        this.isPublished = isPublished;
    }

    public User getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(User ownerUser) {
        this.ownerUser = ownerUser;
    }

    public PricePackage getPricePackage() {
        return pricePackage;
    }

    public void setPricePackage(PricePackage pricePackage) {
        this.pricePackage = pricePackage;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getLastModifiedAt() {
        return lastModifiedAt;
    }

    public void setLastModifiedAt(Date lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    @Override
    public String toString() {
        return "Course{" + "courseID=" + courseID + ", name=" + name + ", briefInformation=" + briefInformation + ", thumbnailImage=" + thumbnailImage + ", description=" + description + ", status=" + status + ", isPublished=" + isPublished + ", ownerUser=" + ownerUser + ", pricePackage=" + pricePackage + ", dimension=" + dimension + ", category=" + category + ", createAt=" + createAt + ", lastModifiedAt=" + lastModifiedAt + ", features=" + features + '}';
    }

}
