/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

public class Dimension {

    private int dimensionID;
    private String name;
    private String type;
    private String description;

    public Dimension() {
    }

    public Dimension(int dimensionID, String name, String type, String description) {
        this.dimensionID = dimensionID;
        this.name = name;
        this.type = type;
        this.description = description;
    }

    public int getDimensionID() {
        return dimensionID;
    }

    public void setDimensionID(int dimensionID) {
        this.dimensionID = dimensionID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Dimension{" + "dimensionID=" + dimensionID + ", name=" + name + ", type=" + type + ", description=" + description + '}';
    }

}
