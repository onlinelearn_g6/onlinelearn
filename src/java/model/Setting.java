/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

public class Setting {

    private int settingID;
    private String name;
    private String mappedValue;
    private String type;
    private int order;
    private boolean status;

    public Setting() {
    }

    public Setting(int settingID, String name, String mappedValue, String type, int order, boolean status) {
        this.settingID = settingID;
        this.name = name;
        this.mappedValue = mappedValue;
        this.type = type;
        this.order = order;
        this.status = status;
    }

    public int getSettingID() {
        return settingID;
    }

    public void setSettingID(int settingID) {
        this.settingID = settingID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMappedValue() {
        return mappedValue;
    }

    public void setMappedValue(String mappedValue) {
        this.mappedValue = mappedValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Setting{" + "settingID=" + settingID + ", name=" + name + ", mappedValue=" + mappedValue + ", type=" + type + ", order=" + order + ", status=" + status + '}';
    }

}
