/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

public class PricePackage {

    private int pricePackageID;
    private String name;
    private int duration;
    private float originalPrice;
    private float salePrice;
    private String description;
    private boolean status;

    public PricePackage() {
    }

    public PricePackage(int pricePackageID, String name, int duration, float originalPrice, float salePrice, String description, boolean status) {
        this.pricePackageID = pricePackageID;
        this.name = name;
        this.duration = duration;
        this.originalPrice = originalPrice;
        this.salePrice = salePrice;
        this.description = description;
        this.status = status;
    }

    public int getPricePackageID() {
        return pricePackageID;
    }

    public void setPricePackageID(int pricePackageID) {
        this.pricePackageID = pricePackageID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public float getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(float originalPrice) {
        this.originalPrice = originalPrice;
    }

    public float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(float salePrice) {
        this.salePrice = salePrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PricePackage{" + "pricePackageID=" + pricePackageID + ", name=" + name + ", duration=" + duration + ", originalPrice=" + originalPrice + ", salePrice=" + salePrice + ", description=" + description + ", status=" + status + '}';
    }

}
