/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

public class Registration {

    private int registrationID;
    private User user;
    private Course course;

    public Registration() {
    }

    public Registration(int registrationID, User user, Course course) {
        this.registrationID = registrationID;
        this.user = user;
        this.course = course;
    }

    public int getRegistrationID() {
        return registrationID;
    }

    public void setRegistrationID(int registrationID) {
        this.registrationID = registrationID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Registration{" + "registrationID=" + registrationID + ", user=" + user + ", course=" + course + '}';
    }

}
