/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;
import java.sql.Blob;
import java.util.Arrays;
import java.util.List;

public class User {

    private int userID;
    private String email;
    private String password;
    private String fullname;
    private Blob avatar;
    private String gender;
    private String mobile;
    private String address;
    private boolean status;
    private String privateToken;
    private Date tokenValidTo;
    private UserRole role;
    private String favorContact;
    private List<String> mobileSplits;

    public User() {
    }

    public User(int userID, String email, String password, String fullname, Blob avatar, String gender, String mobile, String address, boolean status, String privateToken, Date tokenValidTo, UserRole role, String favorContact) {
        this.userID = userID;
        this.email = email;
        this.password = password;
        this.fullname = fullname;
        this.avatar = avatar;
        this.gender = gender;
        this.mobile = mobile;
        this.address = address;
        this.status = status;
        this.privateToken = privateToken;
        this.tokenValidTo = tokenValidTo;
        this.role = role;
        this.favorContact = favorContact;
        this.mobileSplits = splitMobile();
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Blob getAvatar() {
        return avatar;
    }

    public void setAvatar(Blob avatar) {
        this.avatar = avatar;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
        this.mobileSplits = splitMobile();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getPrivateToken() {
        return privateToken;
    }

    public void setPrivateToken(String privateToken) {
        this.privateToken = privateToken;
    }

    public Date getTokenValidTo() {
        return tokenValidTo;
    }

    public void setTokenValidTo(Date tokenValidTo) {
        this.tokenValidTo = tokenValidTo;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getFavorContact() {
        return favorContact;
    }

    public void setFavorContact(String favorContact) {
        this.favorContact = favorContact;
    }

    public List<String> getMobileSplits() {
        return mobileSplits;
    }

    @Override
    public String toString() {
        return "User{" + "userID=" + userID + ", email=" + email + ", password=" + password + ", fullname=" + fullname + ", avatar=" + avatar + ", gender=" + gender + ", mobile=" + mobile + ", address=" + address + ", status=" + status + ", privateToken=" + privateToken + ", tokenValidTo=" + tokenValidTo + ", role=" + role + ", favorContact=" + favorContact + '}';
    }

    private List<String> splitMobile() {
        String mobileClean = mobile.trim().replaceAll(" ", "");
        return Arrays.asList(mobileClean.split(","));
    }

}
