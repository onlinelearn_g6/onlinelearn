<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Not found</title>

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">

        <!-- Vendor CSS Files -->
        <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    </head>
    <body>
        <main>
            <div class="container">
                <section class="section error-404 min-vh-100 d-flex flex-column align-items-center justify-content-center">
                    <h1>401</h1>
                    <h2>Permission Denied!</h2>
                    <c:if test="${sessionScope.user == null}">
                        <a class="btn btn-primary rounded-pill" href="${pageContext.request.contextPath}/login">Login Now</a>
                    </c:if>
                    <c:if test="${sessionScope.user != null}">
                        <button class="btn" onclick="history.back()">Go back</button>
                    </c:if>
                    <img src="${pageContext.request.contextPath}/assets/img/not-found.svg" class="img-fluid py-5" alt="Page Not Found">
                </section>

            </div>
        </main><!-- End #main -->
    </body>
</html>
