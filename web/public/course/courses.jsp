<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Course List</title>

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">
    </head>
    <body>
        <%@include file="../../layout/header.jsp" %>
        <%@include file="../sidebar.jsp" %>

            <main id="main" class="main">
                <div class="pagetitle">
                    <h1>Courses</h1>
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
                        <li class="breadcrumb-item active">Courses</li>
                    </ol>
                </nav>
            </div><!-- End Page Title -->

            <form action="${pageContext.request.contextPath}${url}" method="GET">
                <input type="submit" hidden />
                <input class="d-none d-md-flex mb-4 form-control" type="search" name="search" placeholder="Enter course information" value="${search}"> 

                <div class="row align-items-top">                    
                    <div class="col-lg-10">
                        <c:if test="${requestScope.courses.isEmpty()}">
                            <p class="text-center fs-4">Can not find any courses!</p>
                        </c:if>

                        <section class="section">
                            <c:forEach var="course" items="${requestScope.courses}" varStatus="loop">
                                <c:if test="${loop.index % 4 == 0}">
                                    <div class="row">
                                    </c:if>
                                    <div class="col-lg-3">
                                        <div class="card">
                                            <img src="${course.thumbnailImage}" class="card-img-top w-100" alt="${course.name}">
                                            <div class="card-body">
                                                <h5 class="card-title oneline">
                                                    <a href="${pageContext.request.contextPath}/p/course/detail?courseID=${course.courseID}" class="card-title">
                                                        ${course.name}
                                                    </a>
                                                </h5>
                                                <p class="card-text twoline">${course.briefInformation}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <c:if test="${loop.index % 4 == 3}">
                                    </div>
                                </c:if>
                            </c:forEach>
                        </section>
                    </div>

                    <input type="hidden" name="dimension" value="${dimension}"/>
                    <input type="hidden" name="category" value="${category}"/>
                    <input type="hidden" name="feature" value="${feature}"/>
                    <div class="col-lg-2">
                        <div class="card">
                            <div class="card-body">
                                <!--List dimensions-->
                                <h5 class="card-title pb-0" style="font-weight: bolder; color: #666">Dimension</h5>
                                <c:forEach var="dimensionItem" items="${requestScope.dimensions}">
                                    <h6 class="card-subtitle ms-3 mt-2"
                                        <c:if test="${dimension != null && dimension == dimensionItem.dimensionID}">
                                            style="font-weight: bolder;"
                                        </c:if>>
                                        <a class="text-body-secondary" href="${pageContext.request.contextPath}${url}?itemsPerPage=${page.itemsPerPage}&dimension=${dimensionItem.dimensionID}">${dimensionItem.name}</a>
                                    </h6>
                                </c:forEach>

                                <!--List categorys-->
                                <h5 class="card-title pb-0" style="font-weight: bolder; color: #666">Category</h5>
                                <c:forEach var="categoryItem" items="${requestScope.categorys}">
                                    <h6 class="card-subtitle ms-3 mt-2"
                                        <c:if test="${category != null && category == categoryItem.categoryID}">
                                            style="font-weight: bolder;"
                                        </c:if>>
                                        <a class="text-body-secondary" href="${pageContext.request.contextPath}${url}?itemsPerPage=${page.itemsPerPage}&category=${categoryItem.categoryID}">${categoryItem.name}</a>
                                    </h6>
                                </c:forEach>

                                <!--List features-->
                                <h5 class="card-title pb-0" style="font-weight: bolder; color: #666">Feature</h5>
                                <c:forEach var="featureItem" items="${requestScope.features}">
                                    <h6 class="card-subtitle ms-3 mt-2"
                                        <c:if test="${feature != null && feature == featureItem.featureID}">
                                            style="font-weight: bolder;"
                                        </c:if>>
                                        <a class="text-body-secondary" href="${pageContext.request.contextPath}${url}?itemsPerPage=${page.itemsPerPage}&feature=${featureItem.featureID}">${featureItem.name}</a>
                                    </h6>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-between">
                    <div class="form-group">
                        <input type="text" class="form-control text-center" style="width: 60px" name="itemsPerPage" value="${page.itemsPerPage}"/>
                    </div>

                    <nav aria-label="Pagination">
                        <ul class="pagination justify-content-between">
                            <li class="page-item ${page.previous == page.currentPage ? 'disabled' : ''}">
                                <a class="page-link" href="${pageContext.request.contextPath}${url}?page=${page.previous}&itemsPerPage=${page.itemsPerPage}&search=${search}&dimension=${dimension}&category=${category}&feature=${feature}" tabindex="-1" aria-disabled="true">Previous</a>
                            </li>
                            <c:forEach var="i" begin="1" end="${page.totalPage}">
                                <li class="page-item ${page.currentPage == i ? 'active' : ''}">
                                    <a class="page-link" href="${pageContext.request.contextPath}${url}?page=${i}&itemsPerPage=${page.itemsPerPage}&search=${search}&dimension=${dimension}&category=${category}&feature=${feature}">${i}</a>
                                </li>
                            </c:forEach>
                            <li class="page-item ${page.next == page.currentPage ? 'disabled' : ''}">
                                <a class="page-link" href="${pageContext.request.contextPath}${url}?page=${page.next}&itemsPerPage=${page.itemsPerPage}&search=${search}&dimension=${dimension}&category=${category}&feature=${feature}">Next</a>
                            </li>
                        </ul>
                    </nav><!-- End Disabled and active states -->
                </div>
            </form>

        </main><!-- End #main -->

        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
