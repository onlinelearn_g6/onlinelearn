<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Course Detail</title>

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">
    </head>
    <body>
        <%@include file="../../layout/header.jsp" %>
        <%@include file="../sidebar.jsp" %>

        <main id="main" class="main">
            <div class="pagetitle">
                <h1>Course Detail</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
                        <li class="breadcrumb-item active">Course Detail</li>
                    </ol>
                </nav>
            </div><!-- End Page Title -->

            <c:if test="${successMsg != null || errorMsg != null}">
                <div class="alert alert-success ${successMsg != null ? 'bg-success' : 'bg-danger'} text-light alert-dismissible fade show alertMsg" role="alert" id="msgAlert">
                    ${successMsg}${errorMsg}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                <script>
                    function hideAlert() {
                        $('#msgAlert').hide(2000);
                    }
                    setTimeout(hideAlert, 5000);
                </script>
            </c:if>

            <div class="row align-items-top">                    
                <div class="col-lg-9">
                    <div class="card mb-3">
                        <img src="${course.thumbnailImage}" class="card-img-top w-100" alt="${course.name}">
                        <div class="card-body">
                            <h3 class="mt-3 text-primary fw-semibold">${course.name}</h3>
                            <h5 class="card-title">${course.briefInformation}</h5>
                            <ul>
                                <li class="mb-3">
                                    <span class="fw-bold me-3">Dimension:</span> ${course.dimension.name}
                                </li>
                                <li class="mb-3">
                                    <span class="fw-bold me-3">Dimension Type:</span> ${course.dimension.type}
                                </li>
                                <li class="mb-3">
                                    <span class="fw-bold me-3">Category:</span> ${course.category.name}
                                </li>
                                <li class="mb-3">
                                    <span class="fw-bold me-3">Features:</span>
                                    <c:forEach var="feature" items="${course.features}">
                                        <p class="ms-5 mb-2">${feature.name}</p>
                                    </c:forEach>
                                </li>
                                <li class="mb-3">
                                    <span class="fw-bold me-3">Duration:</span> ${course.pricePackage.duration}
                                </li>
                                <li class="mb-3">
                                    <span class="fw-bold me-3">Price:</span>
                                    <span class="text-decoration-line-through me-3">${course.pricePackage.originalPrice}</span>
                                    <span class="fw-bold fs-4 text-info-emphasis">${course.pricePackage.salePrice}</span>
                                </li>
                            </ul>
                            <p class="card-text ">${course.description}</p>
                        </div>
                    </div>

                    <button type="button" class="btn btn-link" style="font-weight: bold; color: red;" onclick="history.back()">Back</button>
                </div>

                <div class="col-lg-3">
                    <div class="card mb-3">
                        <form action="${pageContext.request.contextPath}/p/course/detail" method="POST">
                            <input type="hidden" name="courseID" value="${course.courseID}"/>
                            <div class="card-header text-center ${registration != null || successMsg != null ? 'text-success' : ''} fs-5 fw-bold">Registration Information</div>
                            <div class="card-body">
                                <label for="email" class="form-label card-title pb-0">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="${user.email}" placeholder="nguyenvana@gmail.com" ${sessionScope.user == null && successMsg == null ? 'required' : 'readonly'}>

                                <label for="fullname" class="form-label card-title pb-0">Full Name</label>
                                <input type="text" class="form-control" id="fullname" name="fullname" value="${user.fullname}" placeholder="Nguyễn Văn A" ${sessionScope.user == null && successMsg == null ? 'required' : 'readonly'}>

                                <label for="mobile" class="form-label card-title pb-0">Mobile</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" value="${user.mobile}" placeholder="0123456789" aria-describedby="passwordHelpBlock" ${sessionScope.user == null && successMsg == null ? 'required' : 'readonly'}>
                                <c:if test="${sessionScope.user == null && successMsg == null}">
                                    <div id="passwordHelpBlock" class="form-text">* You can enter multiple mobile phone numbers, separated by commas (,)</div>
                                </c:if>

                                <label for="gender" class="form-label card-title pb-0">Gender</label>
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" id="gender1" name="gender" value="Male" ${user.gender == null || user.gender == "Male" ? 'checked' : ''} ${sessionScope.user == null && successMsg == null ? '' : 'disabled'}>
                                    <label class="form-check-label me-5" for="gender1">Male</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" id="gender2" name="gender" value="Female" ${user.gender == "Female" ? 'checked' : ''}  ${sessionScope.user == null && successMsg == null ? '' : 'disabled'}>
                                    <label class="form-check-label" for="gender2">Female</label>
                                </div>

                                <label id="favorContact" class="form-label card-title pb-0">Favourite Contact Method</label>
                                <div class="dropdown">
                                    <select class="form-select" id="favorContact" name="favorContact" ${registration == null && successMsg == null ? '' : 'disabled'}>
                                        <option value="Phone" ${user.favorContact == "Phone" ? 'selected' : ''}>Phone</option>
                                        <option value="Email" ${user.favorContact == "Email" ? 'selected' : ''}>Email</option>
                                    </select>
                                </div>

                                <c:if test="${registration == null && successMsg == null}">
                                    <button class="btn btn-primary w-100 mt-3" type="submit">Register Course</button>
                                </c:if>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </main><!-- End #main -->

        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
