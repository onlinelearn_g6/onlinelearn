<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">

        <!-- Vendor CSS Files -->
        <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Template Main CSS File -->
        <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/css/onlinelearn.css" rel="stylesheet">

    </head>

    <body>

        <!-- ======= Header ======= -->
        <header id="header" class="header fixed-top d-flex align-items-center" style="z-index: 1">

            <div class="d-flex align-items-center justify-content-between">
                <a href="${pageContext.request.contextPath}" class="logo d-flex align-items-center">
                    <img src="${pageContext.request.contextPath}/assets/img/logo.png" alt="">
                    <span class="d-none d-lg-block">OnlineLearn</span>
                </a>
                <i class="bi bi-list toggle-sidebar-btn"></i>
            </div><!-- End Logo -->

            <c:if test = "${sessionScope.user != null}">
                <nav class="header-nav ms-auto">
                    <ul class="d-flex align-items-center">
                        <li class="nav-item dropdown pe-3">

                            <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                                <c:if test="${sessionScope.user.avatar == null}">
                                    <img src="${pageContext.request.contextPath}/assets/img/profile-img.jpg" alt="Profile" class="rounded-circle">
                                </c:if>
                                <c:if test="${sessionScope.user.avatar != null}">
                                    <img src="${sessionScope.user.avatar}" alt="Profile" class="rounded-circle">
                                </c:if>
                                <span class="d-none d-md-block dropdown-toggle ps-2">${sessionScope.user.fullname}</span>
                            </a><!-- End Profile Iamge Icon -->

                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                                <li class="dropdown-header">
                                    <h6>${sessionScope.user.fullname}</h6>
                                    <span>${sessionScope.user.role.name}</span>
                                </li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li>
                                    <c:if test="${sessionScope.user != null}">
                                        <a class="dropdown-item d-flex align-items-center" href="${pageContext.request.contextPath}/profile">
                                            <i class="bi bi-person"></i>
                                            <span>My Profile</span>
                                        </a>
                                        <a class="dropdown-item d-flex align-items-center" href="${pageContext.request.contextPath}/profile/chgpwd">
                                            <i class="bi bi-gear"></i>
                                            <span>Change Password</span>
                                        </a>
                                    </c:if>
                                </li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>

                                <li>
                                    <a class="dropdown-item d-flex align-items-center" href="${pageContext.request.contextPath}/logout">
                                        <i class="bi bi-box-arrow-right"></i>
                                        <span>Sign Out</span>
                                    </a>
                                </li>

                            </ul><!-- End Profile Dropdown Items -->
                        </li><!-- End Profile Nav -->

                    </ul>
                </nav><!-- End Icons Navigation -->
            </c:if>

            <c:if test = "${sessionScope.user == null}">
                <nav class="header-nav ms-auto me-3">
                    <a class="btn btn-primary rounded-pill" style="min-width: 90px" href="${pageContext.request.contextPath}/login">Login</a>
                    <a class="btn btn-primary rounded-pill" style="min-width: 90px" href="${pageContext.request.contextPath}/register">Register</a>
                </nav>
            </c:if>

        </header><!-- End Header -->
    </body>
</html>