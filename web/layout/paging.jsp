<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="d-flex justify-content-between">
        <div class="form-group">
            <form class="d-none d-md-flex mb-4" action="${pageContext.request.contextPath}${url}" method="GET">
                <input type="hidden" name="search" value="${search}">
                <input type="text" class="form-control text-center" style="width: 60px" name="itemsPerPage" value="${page.itemsPerPage}"/>
            </form>
        </div>

        <nav aria-label="Pagination">
            <ul class="pagination justify-content-between">
                <li class="page-item ${page.previous == page.currentPage ? 'disabled' : ''}">
                    <a class="page-link" href="${pageContext.request.contextPath}${url}?page=${page.previous}&itemsPerPage=${page.itemsPerPage}&search=${search}" tabindex="-1" aria-disabled="true">Previous</a>
                </li>
                <c:forEach var="i" begin="1" end="${page.totalPage}">
                    <li class="page-item ${page.currentPage == i ? 'active' : ''}">
                        <a class="page-link" href="${pageContext.request.contextPath}${url}?page=${i}&itemsPerPage=${page.itemsPerPage}&search=${search}">${i}</a>
                    </li>
                </c:forEach>
                <li class="page-item ${page.next == page.currentPage ? 'disabled' : ''}">
                    <a class="page-link" href="${pageContext.request.contextPath}${url}?page=${page.next}&itemsPerPage=${page.itemsPerPage}&search=${search}">Next</a>
                </li>
            </ul>
        </nav><!-- End Disabled and active states -->
    </div>
</html>
