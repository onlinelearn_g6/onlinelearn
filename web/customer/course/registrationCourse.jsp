<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My registration course</title>

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">
    </head>
    <body>
        <%@include file="../../layout/header.jsp" %>
        <%@include file="../sidebar.jsp" %>

        <main id="main" class="main">

            <div class="pagetitle">
                <h1>My registration course</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
                        <li class="breadcrumb-item active">My registration course</li>
                    </ol>
                </nav>
            </div><!-- End Page Title -->

            <section class="section">
                <div class="row">
                    <div class="col-lg-12">
                        <section class="section">
                            <c:forEach var="course" items="${requestScope.courses}" varStatus="loop">
                                <c:if test="${loop.index % 4 == 0}">
                                    <div class="row">
                                    </c:if>
                                    <div class="col-lg-3">
                                        <div class="card">
                                            <img src="${course.thumbnailImage}" class="card-img-top w-100" alt="${course.name}">
                                            <div class="card-body">
                                                <h5 class="card-title oneline">
                                                    <a href="${pageContext.request.contextPath}/p/course/detail?courseID=${course.courseID}" class="card-title">
                                                        ${course.name}
                                                    </a>
                                                </h5>
                                                <p class="card-text twoline">${course.briefInformation}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <c:if test="${loop.index % 4 == 3}">
                                    </div>
                                </c:if>
                            </c:forEach>
                        </section>
                    </div>
                </div>
            </section>

        </main><!-- End #main -->

        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
