<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <!-- ======= Sidebar ======= -->
        <aside id="sidebar" class="sidebar">
            <ul class="sidebar-nav" id="sidebar-nav">
                <li class="nav-item">
                    <a class="nav-link ${requestScope.active == 'course'?'':'collapsed'}" href="${pageContext.request.contextPath}/p/courses">
                        <i class="bi bi-book"></i>
                        <span>Courses</span>
                    </a>
                </li>
                <li class="nav-item">
                    <!--<a class="nav-link ${requestScope.active == 'blog'?'':'collapsed'}" href="${pageContext.request.contextPath}/p/blog">-->
                    <a class="nav-link ${requestScope.active == 'blog'?'':'collapsed'}" href="#">
                        <i class="bi bi-newspaper"></i>
                        <span>Blogs</span>
                    </a>
                </li>
                <c:if test="${sessionScope.user != null}">
                    <li class="nav-item">
                        <a class="nav-link ${requestScope.active == 'registration'?'':'collapsed'}" href="${pageContext.request.contextPath}/c/course/registration">
                            <i class="bi bi-newspaper"></i>
                            <span>My registration course</span>
                        </a>
                    </li>
                </c:if>
            </ul>
        </aside><!-- End Sidebar-->
    </body>
</html>
