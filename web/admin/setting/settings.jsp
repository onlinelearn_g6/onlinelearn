<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Settings</title>

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">
    </head>
    <body>
        <%@include file="../../layout/header.jsp" %>
        <%@include file="../sidebar.jsp" %>

        <main id="main" class="main">

            <div class="pagetitle">
                <h1>Manage Settings</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Settings</li>
                    </ol>
                </nav>
            </div><!-- End Page Title -->

            <section class="section">
                <div class="row">
                    <div class="col-lg-12">

                        <c:if test="${createNewMsg != null}">
                            <div class="alert alert-success bg-success text-light alert-dismissible fade show alertMsg" role="alert" id="msgAlert">
                                ${createNewMsg}
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                            <script>
                                function hideAlert() {
                                    $('#msgAlert').hide(2000);
                                }
                                setTimeout(hideAlert, 5000);
                            </script>
                        </c:if>

                        <div class="card">
                            <div class="card-body">
                                <div class="text-end">
                                    <a class="btn btn-primary mb-3 mt-3" href="${pageContext.request.contextPath}/ad/createSetting">Add new setting</a>
                                </div>
                                <div class="text-end">
                                    <div class="dropdown mb-3">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
                                                data-bs-toggle="dropdown" aria-expanded="false">
                                            Shown Columns
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="name" checked/>
                                                        <label class="form-check-label" for="name">Name</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="mappedvalue" checked/>
                                                        <label class="form-check-label" for="mappedvalue">Mapped Value</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="type" checked/>
                                                        <label class="form-check-label" for="type">Type</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="order" checked/>
                                                        <label class="form-check-label" for="                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="order" checked/>Order</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="status" checked/>
                                                        <label class="form-check-label" for="status">Status</label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <script>
                                        function toggleColumn(checkboxElm) {
                                            if ($("#" + checkboxElm.id).is(":checked")) {
                                                $("." + checkboxElm.id).show();
                                            } else {
                                                $("." + checkboxElm.id).hide();
                                            }
                                        }</script>
                                </div>

                                <form class="d-none d-md-flex mb-4" action="${pageContext.request.contextPath}/ad/settings" method="GET">
                                    <input class="form-control" type="search" name="search" placeholder="Enter setting information" value="${search}"> 
                                    <input class="form-control" type="hidden" name="itemsPerPage" value="${itemsPerPage}"> 
                                </form>

                                <!-- Table with stripped rows -->
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center" scope="col">No.</th>
                                            <th class="text-center name" scope="col">Name</th>
                                            <th class="text-center mappedvalue" scope="col">Mapped Value</th>
                                            <th class="text-center type" scope="col">Type</th>
                                            <th class="text-center order" scope="col">Order</th>
                                            <th class="text-center status" scope="col">Status</th>
                                            <th class="text-center" colspan="2">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${requestScope.settings}" var="item" varStatus="loop">
                                            <tr>
                                                <td class="text-center">${(page.currentPage - 1) * page.itemsPerPage + loop.index + 1}</td>
                                                <td class="name">${item.name}</td>
                                                <td class="mappedvalue">${item.mappedValue}</td>
                                                <td class="text-center type">${item.type}</td>
                                                <td class="text-center order">${item.order}</td>
                                                <td class="text-center status">
                                                    <c:if test="${item.status}">
                                                        <span class="badge bg-success">Active</span>
                                                    </c:if>
                                                    <c:if test="${!item.status}">
                                                        <span class="badge bg-danger">Deactive</span>
                                                    </c:if>
                                                </td>
                                                <td><a class="btn btn-link" href="${pageContext.request.contextPath}/ad/updateSetting?settingID=${item.settingID}">View</a></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <!-- End Table with stripped rows -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <%@include file="../../layout/paging.jsp" %>

        </main><!-- End #main -->

        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
