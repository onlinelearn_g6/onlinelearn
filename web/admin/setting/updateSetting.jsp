<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Update Setting</title>

    <!-- Favicons -->
    <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">

    <body>
        <%@include file="../../layout/header.jsp" %>
        <%@include file="../sidebar.jsp" %>
        <main id="main" class="main">

            <div class="pagetitle">
                <h1>Update Setting</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/ad/users">Manage Settings</a></li>
                        <li class="breadcrumb-item active">Update Setting</li>
                    </ol>
                </nav>
            </div><!-- End Page Title -->

            <section class="section">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="card">
                            <div class="card-body">

                                <h5 class="card-title">Update Setting</h5>

                                <c:if test="${updateMsg != null}">
                                    <div class="alert alert-success bg-success text-light alert-dismissible fade show alertMsg" role="alert" id="msgAlert">
                                        ${updateMsg}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                    <script>
                                        function hideAlert() {
                                            $('#msgAlert').hide(2000);
                                        }
                                        setTimeout(hideAlert, 5000);
                                    </script>
                                </c:if>

                                <form class="row g-3" action="${pageContext.request.contextPath}/ad/updateSetting" method="post">
                                    <input type="hidden" name="settingID" value="${requestScope.setting.settingID}"/>
                                    
                                    <div class="col-12">
                                        <label for="name" class="form-label">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="${requestScope.setting.name}" placeholder="Student" required>
                                    </div>

                                    <div class="col-12">
                                        <label for="mappedValue" class="form-label">Mapped Value</label>
                                        <input type="text" class="form-control" id="mappedValue" name="mappedValue" value="${requestScope.setting.mappedValue}" placeholder="Follow" required>
                                    </div>

                                    <div class="col-12">
                                        <label for="type" class="form-label">Type</label>
                                        <input type="text" class="form-control" id="type" name="type" value="${requestScope.setting.type}" placeholder="User Role" required>
                                    </div>

                                    <div class="col-12">
                                        <label for="order" class="form-label">Order</label>
                                        <input type="number" class="form-control" id="order" name="order" value="${requestScope.setting.order}" placeholder="1" required>
                                    </div>
                                    
                                    <div class="col-12 d-flex">
                                        <label class="form-label me-3">Status</label>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" name="status" ${setting.status ? 'checked' : ''}>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button class="btn btn-primary w-100" type="submit">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- End #main -->     
        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
