<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Account</title>

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">
    </head>
    <body>
        <%@include file="../../layout/header.jsp" %>
        <%@include file="sidebar.jsp" %>

        <main id="main" class="main">
            <section class="section error-404 min-vh-100 d-flex flex-column align-items-center justify-content-center">
                <img src="${pageContext.request.contextPath}/assets/img/logo.png" width="150px" height="150px" alt="Logo image"/>
                <h2 class="mt-5">Welcome To <b>OnlineLearn</b> System!</h2>
            </section>
        </main><!-- End #main -->

        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
