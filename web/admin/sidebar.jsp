<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <!-- ======= Sidebar ======= -->
        <aside id="sidebar" class="sidebar">
            <ul class="sidebar-nav" id="sidebar-nav">
                <li class="nav-item">
                    <a class="nav-link ${requestScope.active == 'user'?'':'collapsed'}" href="${pageContext.request.contextPath}/ad/users">
                        <i class="bi bi-people"></i>
                        <span>Manage Users</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ${requestScope.active == 'setting'?'':'collapsed'}" href="${pageContext.request.contextPath}/ad/settings">
                        <i class="bi bi-people"></i>
                        <span>Manage Settings</span>
                    </a>
                </li>
            </ul>
        </aside><!-- End Sidebar-->
    </body>
</html>
