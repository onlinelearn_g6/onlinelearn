<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Update User</title>

    <!-- Favicons -->
    <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">

    <body>
        <%@include file="../../layout/header.jsp" %>
        <%@include file="../sidebar.jsp" %>
        <main id="main" class="main">

            <div class="pagetitle">
                <h1>Update User</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/ad/users">Manage Users</a></li>
                        <li class="breadcrumb-item active">Update User</li>
                    </ol>
                </nav>
            </div><!-- End Page Title -->

            <section class="section">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="card">
                            <div class="card-body">

                                <h5 class="card-title">Update User</h5>

                                <c:if test="${updateMsg != null}">
                                    <div class="alert alert-success bg-success text-light alert-dismissible fade show alertMsg" role="alert" id="msgAlert">
                                        ${updateMsg}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                    <script>
                                        function hideAlert() {
                                            $('#msgAlert').hide(2000);
                                        }
                                        setTimeout(hideAlert, 5000);
                                    </script>
                                </c:if>

                                <form class="row g-3" action="${pageContext.request.contextPath}/ad/updateUser" method="post">

                                    <input type="hidden" name="userID" value="${user.userID}" />

                                    <div class="col-12">
                                        <label class="form-label">Full Name</label>
                                        <input type="text" class="form-control" value="${user.fullname}" disabled>
                                    </div>

                                    <div class="col-12">
                                        <label class="form-label">Email</label>
                                        <input type="email" class="form-control" value="${user.email}" disabled>
                                    </div>

                                    <div class="col-12">
                                        <label class="form-label">Mobile</label>
                                        <input type="text" class="form-control" value="${user.mobile}" disabled>
                                    </div>

                                    <div class="col-12">
                                        <label class="form-label">Gender</label>
                                        <input type="text" class="form-control" value="${user.gender}" disabled>
                                    </div>

                                    <div class="col-12">
                                        <label class="form-label">Address</label>
                                        <input type="text" class="form-control" value="${user.address}" disabled>
                                    </div>
                                    
                                    <div class="col-12">
                                        <label id="favorContact" class="form-label">Favourite Contact Method</label>
                                        <div class="dropdown">
                                            <select class="btn btn-outline-secondary" id="favorContact" name="favorContact">
                                                <option value="Phone" ${user.favorContact == "Phone" ? 'selected' : ''}>Phone</option>
                                                <option value="Email" ${user.favorContact == "Email" ? 'selected' : ''}>Email</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <label id="roleID" class="form-label">Role</label>
                                        <div class="dropdown">
                                            <select class="btn btn-outline-secondary" id="roleID" name="roleID">
                                                <c:forEach items="${userRoles}" var="role">
                                                    <option value="${role.id}" ${role.id == user.role.id ? 'selected' : ''}>${role.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12 d-flex">
                                        <label class="form-label me-3">Status</label>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" name="status" ${user.status ? 'checked' : ''}>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button class="btn btn-primary w-100" type="submit">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- End #main -->     
        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
