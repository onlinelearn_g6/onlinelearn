<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Users</title>

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">
    </head>
    <body>
        <%@include file="../../layout/header.jsp" %>
        <%@include file="../sidebar.jsp" %>

        <main id="main" class="main">

            <div class="pagetitle">
                <h1>Manage Users</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Users</li>
                    </ol>
                </nav>
            </div><!-- End Page Title -->

            <section class="section">
                <div class="row">
                    <div class="col-lg-12">

                        <c:if test="${createNewMsg != null}">
                            <div class="alert alert-success bg-success text-light alert-dismissible fade show alertMsg" role="alert" id="msgAlert">
                                ${createNewMsg}
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                            <script>
                                function hideAlert() {
                                    $('#msgAlert').hide(2000);
                                }
                                setTimeout(hideAlert, 5000);
                            </script>
                        </c:if>

                        <div class="card">
                            <div class="card-body">
                                <div class="text-end">
                                    <a class="btn btn-primary mb-3 mt-3" href="${pageContext.request.contextPath}/ad/createUser">Add new user</a>
                                </div>
                                <div class="text-end">
                                    <div class="dropdown mb-3">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
                                                data-bs-toggle="dropdown" aria-expanded="false">
                                            Shown Columns
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="fullname" checked/>
                                                        <label class="form-check-label" for="fullname">Full name</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="email" checked/>
                                                        <label class="form-check-label" for="email">Email</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="gender" checked/>
                                                        <label class="form-check-label" for="gender">Gender</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="mobile" checked/>
                                                        <label class="form-check-label" for="                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="mobile" checked/>Mobile</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="address" checked/>
                                                        <label class="form-check-label" for="address">Address</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="role" checked/>
                                                        <label class="form-check-label" for="role">Role</label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown-item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" onchange="toggleColumn(this)" id="status" checked/>
                                                        <label class="form-check-label" for="status">Status</label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <script>
                                        function toggleColumn(checkboxElm) {
                                            if ($("#" + checkboxElm.id).is(":checked")) {
                                                $("." + checkboxElm.id).show();
                                            } else {
                                                $("." + checkboxElm.id).hide();
                                            }
                                        }</script>
                                </div>

                                <form class="d-none d-md-flex mb-4" action="${pageContext.request.contextPath}/ad/users" method="GET">
                                    <input class="form-control" type="search" name="search" placeholder="Enter user information" value="${search}"> 
                                    <input class="form-control" type="hidden" name="itemsPerPage" value="${itemsPerPage}"> 
                                </form>

                                <!-- Table with stripped rows -->
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center" scope="col">No.</th>
                                            <th class="text-center fullname" scope="col">Full Name</th>
                                            <th class="text-center email" scope="col">Email</th>
                                            <th class="text-center gender" scope="col">Gender</th>
                                            <th class="text-center mobile" scope="col">Mobile</th>
                                            <th class="text-center address" scope="col">Address</th>
                                            <th class="text-center role" scope="col">Role</th>
                                            <th class="text-center status" scope="col">Status</th>
                                            <th class="text-center" colspan="2">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${requestScope.users}" var="item" varStatus="loop">
                                            <tr>
                                                <td class="text-center">${(page.currentPage - 1) * page.itemsPerPage + loop.index + 1}</td>
                                                <td class="fullname">${item.fullname}</td>
                                                <td class="email">${item.email}</td>
                                                <td class="text-center gender">${item.gender}</td>
                                                <td class="text-center mobile">
                                                    <c:forEach items="${item.mobileSplits}" var="mobileSplit" varStatus="loop1">
                                                        <c:if test="${loop1.index != 0}"></br></c:if>${mobileSplit}
                                                    </c:forEach>
                                                </td>
                                                <td class="text-center address">${item.address}</td>
                                                <td class="text-center role">${item.role.name}</td>
                                                <td class="text-center status">
                                                    <c:if test="${item.status}">
                                                        <span class="badge bg-success">Active</span>
                                                    </c:if>
                                                    <c:if test="${!item.status}">
                                                        <span class="badge bg-danger">Deactive</span>
                                                    </c:if>
                                                </td>
                                                <td><a class="btn btn-link" href="${pageContext.request.contextPath}/ad/updateUser?userID=${item.userID}">View</a></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <!-- End Table with stripped rows -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <%@include file="../../layout/paging.jsp" %>

        </main><!-- End #main -->

        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
