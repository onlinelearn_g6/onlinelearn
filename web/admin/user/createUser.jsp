<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Create User</title>

    <!-- Favicons -->
    <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">

    <body>
        <%@include file="../../layout/header.jsp" %>
        <%@include file="../sidebar.jsp" %>
        <main id="main" class="main">

            <div class="pagetitle">
                <h1>Update User</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/ad/users">Manage Users</a></li>
                        <li class="breadcrumb-item active">Create User</li>
                    </ol>
                </nav>
            </div><!-- End Page Title -->

            <section class="section">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="card">
                            <div class="card-body">

                                <h5 class="card-title">Create User</h5>

                                <c:if test="${errorMsg != null}">
                                    <div class="alert alert-success bg-danger text-light alert-dismissible fade show alertMsg" role="alert" id="msgAlert">
                                        ${errorMsg}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                    <script>
                                        function hideAlert() {
                                            $('#msgAlert').hide(2000);
                                        }
                                        setTimeout(hideAlert, 5000);
                                    </script>
                                </c:if>

                                <form class="row g-3" action="${pageContext.request.contextPath}/ad/createUser" method="post">
                                    <div class="col-12">
                                        <label for="email" class="form-label">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" value="${requestScope.user.email}" placeholder="nguyenvana@gmail.com" required>
                                    </div>

                                    <div class="col-12">
                                        <label for="fullname" class="form-label">Full Name</label>
                                        <input type="text" class="form-control" id="fullname" name="fullname" value="${requestScope.user.fullname}" placeholder="Nguyễn Văn A" required>
                                    </div>

                                    <div class="col-12">
                                        <label for="mobile" class="form-label">Mobile</label>
                                        <input type="text" class="form-control" id="mobile" name="mobile" value="${requestScope.user.mobile}" placeholder="0123456789" required>
                                        <p class="mt-2"><em>* You can enter multiple mobile phone numbers, separated by commas (,)</em></p>
                                    </div>

                                    <div class="col-12">
                                        <label for="gender" class="form-label">Gender</label>
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" id="gender1" name="gender" value="Male" ${requestScope.user.gender == null || requestScope.user.gender == "Male" ? 'checked' : ''}>
                                            <label class="form-check-label me-5" for="gender1">Male</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" id="gender2" name="gender" value="Female" ${requestScope.user.gender == "Female" ? 'checked' : ''}>
                                            <label class="form-check-label" for="gender2">Female</label>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <label for="address" class="form-label">Address</label>
                                        <input type="text" class="form-control" id="address" name="address" value="${requestScope.user.address}" placeholder="Hà Nội" required>
                                    </div>
                                    
                                    <div class="col-12">
                                        <label for="favorContact" class="form-label">Favourite Contact Method</label>
                                        <div class="dropdown">
                                            <select class="btn btn-outline-secondary" id="favorContact" name="favorContact">
                                                <option value="Phone" ${requestScope.user.favorContact == null || requestScope.user.favorContact == "Email" ? 'selected' : ''}>Phone</option>
                                                <option value="Email" ${requestScope.user.favorContact == "Email" ? 'selected' : ''}>Email</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <label for="roleID" class="form-label">Role</label>
                                        <div class="dropdown">
                                            <select class="btn btn-outline-secondary" id="roleID" name="roleID">
                                                <c:forEach items="${userRoles}" var="role">
                                                    <option value="${role.id}" ${role.id == requestScope.user.role.id ? 'selected' : ''}>${role.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button class="btn btn-primary w-100" type="submit">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- End #main -->     
        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
