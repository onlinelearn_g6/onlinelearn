<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Change Password</title>
    
    <!-- Favicons -->
    <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">
    
    <body>
        <%@include file="../../layout/header.jsp" %>
        <c:if test="${sessionScope.user.role.id == 1}"><%@include file="../../admin/sidebar.jsp" %></c:if>
        
        <main id="main" class="main">

            <div class="pagetitle">
                <h1>Change Password</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
                        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/profile">My Profile</a></li>
                        <li class="breadcrumb-item active">Change Password</li>
                    </ol>
                </nav>
            </div><!-- End Page Title -->

            <section class="section profile">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="card">
                            <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">

                                <img src="${pageContext.request.contextPath}/assets/img/profile-img.jpg" alt="Profile" class="rounded-circle">
                                <h2 class="mt-3">${sessionScope.user.fullname}</h2>
                                <h3 class="mt-1">${sessionScope.user.role.name}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-8">
                        <div class="card">
                            <div class="card-body pt-3">
                                <!-- Bordered Tabs -->
                                <ul class="nav nav-tabs nav-tabs-bordered">
                                    <li class="nav-item">
                                        <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-change-password">Change Password</button>
                                    </li>
                                </ul>

                                <c:if test="${chgpwdMsg != null || chgpwdError != null}">
                                    <div class="alert ${chgpwdMsg != null ? 'alert-success bg-success' : 'alert-danger bg-danger'} text-light alert-dismissible fade show alertMsg" role="alert" id="msgAlert">
                                        ${chgpwdMsg}${chgpwdError}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                    <script>
                                        function hideAlert() {
                                            $('#msgAlert').hide(2000);
                                        }
                                        setTimeout(hideAlert, 5000);
                                    </script>
                                </c:if>

                                <div class="tab-content pt-2">
                                    <div class="tab-pane fade show active profile-change-password" id="profile-change-password">
                                        <!-- Change Password Form -->
                                        <form action="${pageContext.request.contextPath}/profile/chgpwd" method="post">
                                            <div class="row mb-3">
                                                <label for="currentPassword" class="col-md-4 col-lg-3 col-form-label">Current Password</label>
                                                <div class="col-md-8 col-lg-9">
                                                    <input name="password" type="password" class="form-control" id="currentPassword" value="${requestScope.password}" required>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">New Password</label>
                                                <div class="col-md-8 col-lg-9">
                                                    <input name="newpassword" type="password" class="form-control" id="newPassword" value="${requestScope.newpassword}" required>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">Re-enter New Password</label>
                                                <div class="col-md-8 col-lg-9">
                                                    <input name="renewpassword" type="password" class="form-control" id="renewPassword" value="${requestScope.renewpassword}" required>
                                                </div>
                                            </div>

                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary">Change Password</button>
                                            </div>
                                        </form><!-- End Change Password Form -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- End #main -->
        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
