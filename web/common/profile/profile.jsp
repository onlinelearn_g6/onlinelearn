<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>My Profile</title>

    <!-- Favicons -->
    <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">

    <body>
        <%@include file="../../layout/header.jsp" %>
        <c:if test="${sessionScope.user.role.id == 1}"><%@include file="../../admin/sidebar.jsp" %></c:if>
        <c:if test="${sessionScope.user.role.id == 2}"><%@include file="../../customer/sidebar.jsp" %></c:if>

            <main id="main" class="main">

                <div class="pagetitle">
                    <h1>My Profile</h1>
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
                        <li class="breadcrumb-item active">My Profile</li>
                    </ol>
                </nav>
            </div><!-- End Page Title -->

            <section class="section profile">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="card">
                            <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">

                                <img src="${pageContext.request.contextPath}/assets/img/profile-img.jpg" alt="Profile" class="rounded-circle">
                                <h2 class="mt-3">${sessionScope.user.fullname}</h2>
                                <h3 class="mt-1">${sessionScope.user.role.name}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-8">
                        <div class="card">
                            <div class="card-body pt-3">
                                <!-- Bordered Tabs -->
                                <ul class="nav nav-tabs nav-tabs-bordered">
                                    <li class="nav-item">
                                        <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-edit">My Profile</button>
                                    </li>
                                </ul>

                                <c:if test="${updateMsg != null || updateError != null}">
                                    <div class="alert ${updateMsg != null ? 'alert-success bg-success' : 'alert-danger bg-danger'} text-light alert-dismissible fade show alertMsg" role="alert" id="msgAlert">
                                        ${updateMsg}${updateError}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                    <script>
                                        function hideAlert() {
                                            $('#msgAlert').hide(2000);
                                        }
                                        setTimeout(hideAlert, 5000);
                                    </script>
                                </c:if>

                                <div class="tab-content pt-2">
                                    <div class="tab-pane fade show active profile-edit" id="profile-edit">
                                        <form action="${pageContext.request.contextPath}/profile" method="post">
                                            <div class="row mb-3">
                                                <label class="col-md-4 col-lg-3 col-form-label">Email</label>
                                                <div class="col-lg-9 col-md-8 mt-1">
                                                    <input type="text" name="email" class="form-control" value="${sessionScope.user.email}" readonly>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-md-4 col-lg-3 col-form-label">Full Name</label>
                                                <div class="col-lg-9 col-md-8 mt-1">
                                                    <input type="text" name="fullname" class="form-control" value="${sessionScope.user.fullname}">
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-md-4 col-lg-3 col-form-label">Gender</label>
                                                <div class="col-md-8 col-lg-9 mt-1">
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="gender1" name="gender" value="Male" ${user.gender == "Male" ? 'checked' : ''}>
                                                        <label class="me-5 text-secondary" for="gender1">Male</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input type="radio" class="form-check-input" id="gender2" name="gender" value="Female" ${user.gender == "Female" ? 'checked' : ''}>
                                                        <label class="form-check-label text-secondary" for="gender2">Female</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-md-4 col-lg-3 col-form-label">Mobile</label>
                                                <div class="col-lg-9 col-md-8 mt-1">
                                                    <input type="text" name="mobile" class="form-control" value="${sessionScope.user.mobile}">
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-md-4 col-lg-3 col-form-label">Address</label>
                                                <div class="col-lg-9 col-md-8 mt-1">
                                                    <input type="text" name="address" class="form-control" value="${sessionScope.user.address}">
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-md-4 col-lg-3 col-form-label">Role</label>
                                                <div class="col-lg-9 col-md-8 mt-1 fw-bold text-success">${sessionScope.user.role.name}</div>
                                            </div>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary">Update Profile</button>
                                            </div>
                                        </form><!-- End Change Password Form -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- End #main -->
        <%@include file="../../layout/footer.jsp" %>
    </body>
</html>
