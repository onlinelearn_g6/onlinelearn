<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="keywords">
        <title>Forgot password</title>

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">

        <!-- Vendor CSS Files -->
        <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">

    </head>

    <body>

        <main>
            <div class="container">
                <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 col-md-10 d-flex flex-column align-items-center justify-content-center">

                                <div class="d-flex justify-content-center py-4">
                                    <a href="${pageContext.request.contextPath}" class="logo d-flex align-items-center w-auto">
                                        <img src="${pageContext.request.contextPath}/assets/img/logo.png" alt="">
                                        <span class="d-none d-lg-block">OnlineLearn</span>
                                    </a>
                                </div><!-- End Logo -->

                                <div class="card mb-3">
                                    <div class="card-body">
                                        <div class="pt-1 pb-2">
                                            <h5 class="card-title text-center pb-0 fs-4">Reset Your Password</h5>
                                            <p class="text-start">Lost your password? Please enter your email address. You will receive a link to create a new password via email.</p>
                                        </div>
                                        
                                        <form class="row g-3 needs-validation" action="${pageContext.request.contextPath}/fpwd" method="post">
                                            <div class="col-12">
                                                <label for="yourEmail" class="form-label">Email</label>
                                                <input type="email" name="email" class="form-control" id="yourEmail" value="${requestScope.email}" placeholder="abc@gmail.com" required>
                                            </div>
                                                <p class="text-danger">${fpwdError}</p>

                                            <div class="col-12">
                                                <button class="btn btn-primary w-100" type="submit">Reset Password</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main><!-- End #main -->
    </body>
</html>