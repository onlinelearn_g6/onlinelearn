<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="keywords">
        <title>Reset Password</title>

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">

        <!-- Vendor CSS Files -->
        <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">

    </head>

    <body>

        <main>
            <div class="container">
                <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

                                <div class="d-flex justify-content-center py-4">
                                    <a href="${pageContext.request.contextPath}" class="logo d-flex align-items-center w-auto">
                                        <img src="${pageContext.request.contextPath}/assets/img/logo.png" alt="">
                                        <span class="d-none d-lg-block">OnlineLearn</span>
                                    </a>
                                </div><!-- End Logo -->

                                <div class="card mb-3">
                                    <div class="card-body">
                                        <div class="pt-4 pb-2">
                                            <h5 class="card-title text-center pb-0 fs-4">Reset Password</h5>
                                            <p class="text-center" style="color: red;">${resetPwdError}</p>
                                            <c:if test = "${resetPwdError == null}">
                                                <p class="text-center">Enter your new password</p>
                                            </c:if>
                                        </div>

                                        <form class="row g-3 needs-validation" action="${pageContext.request.contextPath}/resetPwd" method="post">
                                            <div class="col-12">
                                                <label for="email" class="form-label">Email</label>
                                                <input type="email" class="form-control" id="email" value="${email}" disabled>
                                                <input type="hidden" name="email" value="${email}">
                                            </div>
                                            <div class="col-12">
                                                <label for="newpassword" class="form-label">New Password</label>
                                                <input type="password" name="newpassword" class="form-control" id="newpassword" value="${requestScope.newpassword}" placeholder="********" required>
                                            </div>
                                            <div class="col-12">
                                                <label for="renewpassword" class="form-label">Re-enter New Password</label>
                                                <input type="password" name="renewpassword" class="form-control" id="renewpassword" value="${requestScope.renewpassword}" placeholder="********" required>
                                            </div>
                                            <div class="col-12 mt-3">
                                                <button class="btn btn-primary w-100" type="submit">Finish</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main><!-- End #main -->
    </body>
</html>