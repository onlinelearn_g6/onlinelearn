<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="${pageContext.request.contextPath}/assets/img/favicon.ico" rel="icon" type="image/x-icon">

        <!-- Vendor CSS Files -->
        <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    </head>
    <body>
        <main>
            <div class="container">
                <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

                                <div class="d-flex justify-content-center py-4">
                                    <a href="${pageContext.request.contextPath}" class="logo d-flex align-items-center w-auto">
                                        <img src="${pageContext.request.contextPath}/assets/img/logo.png" alt="">
                                        <span class="d-none d-lg-block">OnlineLearn</span>
                                    </a>
                                </div><!-- End Logo -->

                                <div class="card mb-3">

                                    <div class="card-body">

                                        <div class="pt-4 pb-2">
                                            <h5 class="card-title text-center pb-0 fs-4">Register New Account</h5>
                                            <c:if test = "${errorMsg != null}">
                                                <p class="text-center" style="color: red;">${errorMsg}</p>
                                            </c:if>
                                            <c:if test = "${errorMsg == null}">
                                                <p class="text-center">Enter your personal details</p>
                                            </c:if>
                                        </div>

                                        <form class="row g-3" action="${pageContext.request.contextPath}/register" method="post">

                                            <div class="col-12">
                                                <label for="yourEmail" class="form-label">Your Email</label>
                                                <input type="email" name="email" class="form-control" id="yourEmail" value="${requestScope.user.email}" placeholder="nguyenvana@gmail.com" required>
                                            </div>

                                            <div class="col-12">
                                                <label for="yourUsername" class="form-label">Username</label>
                                                <input type="text" name="username" class="form-control" id="yourUsername" value="${requestScope.user.getUsername()}" placeholder="nguyenvana" required>
                                            </div>

                                            <div class="col-12">
                                                <label for="yourPassword" class="form-label">Password</label>
                                                <input type="password" name="password" class="form-control" id="yourPassword" value="${requestScope.user.getPassword()}" placeholder="******" required>
                                            </div>

                                            <div class="col-12">
                                                <label for="yourPasswordcf" class="form-label">Confirm Password</label>
                                                <input type="password" name="passwordcf" class="form-control" id="yourPasswordcf" value="${requestScope.passwordcf}" placeholder="*******" required>
                                            </div>

                                            <div class="col-12">
                                                <label for="inputText" class="form-label">First Name</label>
                                                <input type="text" name="firstname" value="${requestScope.user.getFirstname()}" class="form-control" placeholder="A" required>
                                            </div>

                                            <div class="col-12">
                                                <label for="inputPassword" class="form-label">Last Name</label>
                                                <input type="text" name="lastname" value="${requestScope.user.getLastname()}" class="form-control" placeholder="Nguyến Văn" required>
                                            </div>

                                            <div class="col-12">
                                                <label for="inputNumber" class="form-label">Address</label>
                                                <input type="text" name="address" value="${requestScope.user.getAddress()}" class="form-control" placeholder="Hà Nội" required>
                                            </div>

                                            <div class="col-12">
                                                <label for="inputDate" class="form-label">Phone</label>
                                                <input type="text" class="form-control" name="phone" value="${requestScope.user.getPhone()}" placeholder="1234567890" required>
                                            </div>

                                            <fieldset class="col-12">
                                                <legend class="col-form-label col-sm-2 pt-0">Role</legend>
                                                <c:forEach items="${requestScope.roles}" var="item">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="roleID" id="${item.id}" value="${item.id}" required>
                                                        <label class="form-check-label" for="${item.id}">
                                                            ${item.name}
                                                        </label>
                                                    </div>
                                                </c:forEach>
                                            </fieldset>

                                            <div class="col-12">
                                                <button class="btn btn-primary w-100" type="submit">Create Account</button>
                                            </div>

                                            <div class="col-12">
                                                <p class="small mb-0">Already have an account? <a href="${pageContext.request.contextPath}/login">Log in</a></p>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main><!-- End #main -->
    </body>
</html>