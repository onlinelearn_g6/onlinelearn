﻿USE [master]
GO
DROP DATABASE IF EXISTS [OnlineLearn]
GO
CREATE DATABASE [OnlineLearn]
GO
USE [OnlineLearn]
GO

CREATE TABLE UserRole (
	UserRoleID INT IDENTITY(1,1) PRIMARY KEY,
	[Name] NVARCHAR(30)
);

CREATE TABLE [User] (
	UserID INT IDENTITY(1,1) PRIMARY KEY,
	Email VARCHAR(100) NOT NULL UNIQUE,
	[Password] VARCHAR(100),
	FullName NVARCHAR(100),
	Avatar IMAGE,
	Gender VARCHAR(10),
	Mobile VARCHAR(15),
	[Address] NVARCHAR(200),
	[Status] BIT DEFAULT 1,
	PrivateToken VARCHAR(50), --Forgot Password case
	TokenValidTo DATETIME,
	UserRoleID INT FOREIGN KEY REFERENCES UserRole(UserRoleID)
);

CREATE TABLE Slider (
	SliderID INT IDENTITY(1,1) PRIMARY KEY,
	Title NVARCHAR(100),
	[Image] IMAGE,
	Backlink VARCHAR(100),
	Notes NVARCHAR(200),
	[Status] BIT DEFAULT 1
);

CREATE TABLE Category (
	CategoryID INT IDENTITY(1,1) PRIMARY KEY,
	[Name] NVARCHAR(50)
);

CREATE TABLE Feature (
	FeatureID INT IDENTITY(1,1) PRIMARY KEY,
	[Name] NVARCHAR(50)
);

CREATE TABLE Blog (
	BlogID INT IDENTITY(1,1) PRIMARY KEY,
	Title NVARCHAR(100),
	Thumbnail NVARCHAR(200),
	BriefInformation NVARCHAR(200),
	Author NVARCHAR(100),
	[Description] NVARCHAR(MAX),
	[Status] BIT DEFAULT 1,
	CategoryID INT FOREIGN KEY REFERENCES Category(CategoryID),
	CreatedAt DATE DEFAULT GETDATE(),
	LastModifiedAt DATE DEFAULT GETDATE()
);

CREATE TABLE BlogFeature (
	BlogFeatureID INT IDENTITY(1,1) PRIMARY KEY,
	BlogID INT FOREIGN KEY REFERENCES Blog(BlogID),
	FeatureID INT FOREIGN KEY REFERENCES Feature(FeatureID)
);

CREATE TABLE PricePackage (
	PricePackageID INT IDENTITY(1,1) PRIMARY KEY,
	[Name] NVARCHAR(50),
	Duration TINYINT,
	OriginalPrice MONEY,
	SalePrice MONEY,
	[Description] NVARCHAR(MAX),
	[Status] BIT DEFAULT 1
);

CREATE TABLE Dimension (
	DimensionID INT IDENTITY(1,1) PRIMARY KEY,
	[Name] NVARCHAR(50),
	[Type] NVARCHAR(50),
	[Description] NVARCHAR(MAX)
);

CREATE TABLE Course (
	CourseID INT IDENTITY(1,1) PRIMARY KEY,
	[Name] NVARCHAR(50),
	BriefInformation NVARCHAR(200),
	ThumbnailImage IMAGE,
	[Description] NVARCHAR(MAX),
	[Status] BIT DEFAULT 1,
	isPublished BIT DEFAULT 0,
	OwnerUserID INT FOREIGN KEY REFERENCES [User](UserID),
	PricePackageID INT FOREIGN KEY REFERENCES PricePackage(PricePackageID),
	DimensionID INT FOREIGN KEY REFERENCES Dimension(DimensionID),
	CategoryID INT FOREIGN KEY REFERENCES Category(CategoryID),
	CreatedAt DATE DEFAULT GETDATE(),
	LastModifiedAt DATE DEFAULT GETDATE()
)

CREATE TABLE CourseFeature (
	CourseFeatureID INT IDENTITY(1,1) PRIMARY KEY,
	CourseID INT FOREIGN KEY REFERENCES Course(CourseID),
	FeatureID INT FOREIGN KEY REFERENCES Feature(FeatureID)
);

CREATE TABLE CourseDimension (
	CourseDimensionID INT IDENTITY(1,1) PRIMARY KEY,
	CourseID INT FOREIGN KEY REFERENCES Course(CourseID),
	DimensionID INT FOREIGN KEY REFERENCES Dimension(DimensionID)
);

CREATE TABLE Registration (
	RegistrationID INT IDENTITY(1,1) PRIMARY KEY,
	RegistrationTime DATETIME,
	ValidFrom DATETIME,
	ValidTo DATETIME,
	[Status] BIT DEFAULT 1,
	SubmittedStatus VARCHAR(20), --Success, Cancelled, Submitted, Paid
	PaidTime DATETIME,
	Notes NVARCHAR(200),
	CourseID INT FOREIGN KEY REFERENCES Course(CourseID),
	UserID INT FOREIGN KEY REFERENCES [User](UserID),
	CreatedAt DATE DEFAULT GETDATE(),
	LastModifiedAt DATE DEFAULT GETDATE(),
	LastModifiedBy INT FOREIGN KEY REFERENCES [User](UserID)
);

CREATE TABLE Lesson (
	LessonID INT IDENTITY(1,1) PRIMARY KEY,
	[Status] BIT DEFAULT 1,
	CourseID INT FOREIGN KEY REFERENCES Course(CourseID),
);

CREATE TABLE Quiz (
	QuizID INT IDENTITY(1,1) PRIMARY KEY,
	Topic NVARCHAR(200),
	[Level] NVARCHAR(20),
	[Status] BIT DEFAULT 1,
	TotalMark INT,
	PassMark INT,
	CourseID INT FOREIGN KEY REFERENCES Course(CourseID),
	DimensionID INT FOREIGN KEY REFERENCES Dimension(DimensionID),
	LessonID INT FOREIGN KEY REFERENCES Lesson(LessonID)
);

CREATE TABLE Question (
	QuestionID INT IDENTITY(1,1) PRIMARY KEY,
	Question NVARCHAR(200),
	Media VARBINARY(MAX),
	MediaType NVARCHAR(20), --Image, Video, Audio
	Explanation NVARCHAR(200),
	[Level] NVARCHAR(20),
	[Status] BIT DEFAULT 1,
	QuizID INT FOREIGN KEY REFERENCES Quiz(QuizID)
);

CREATE TABLE Answer (
	AnswerID INT IDENTITY(1,1) PRIMARY KEY,
	Content NVARCHAR(100),
	Explanation NVARCHAR(200),
	IsCorrect BIT,
	QuizID INT FOREIGN KEY REFERENCES Quiz(QuizID)
);

CREATE TABLE Result (
	ResultID INT IDENTITY(1,1) PRIMARY KEY,
	Duration INT, --ms
	Score REAL,
	ExamStart DATETIME,
	ExamEnd DATETIME,
	QuizID INT FOREIGN KEY REFERENCES Quiz(QuizID),
	UserID INT FOREIGN KEY REFERENCES [User](UserID)
);

CREATE TABLE Setting (
	SettingID INT IDENTITY(1,1) PRIMARY KEY,
	[Name] NVARCHAR(50),
	[Mapped Value] NVARCHAR(100),
	[Type] NVARCHAR(50),
	[Order] INT,
	[Status] BIT DEFAULT 1
);

USE [OnlineLearn]
GO

INSERT INTO UserRole ([Name])
VALUES (N'Admin'),
(N'Customer'),
(N'Marketing Staff'),
(N'Sale Staff'),
(N'Expert');

USE [OnlineLearn]
GO

INSERT INTO [dbo].[User] ([Email], [Password], [FullName], [Avatar], [Gender], [Mobile], [Address], [Status], [PrivateToken], [TokenValidTo], [UserRoleID])
VALUES ('lamlv@gmail.com','202cb962ac59075b964b07152d234b70',N'Lê Văn Lâm',NULL,'Male','0123321123',N'Hà Nội',1,NULL,NULL,1),
('lannt@gmail.com','202cb962ac59075b964b07152d234b70',N'Ngô Thuỳ Lan',NULL,'Female','0965854412',N'Lạng Sơn',1,NULL,NULL,2),
('phuongdt@gmail.com','202cb962ac59075b964b07152d234b70',N'Đinh Thuỳ Phương',NULL,'Female','0912232458',N'Thanh Hoá',1,NULL,NULL,2),
('ducha@gmail.com','202cb962ac59075b964b07152d234b70',N'Hoàng Anh Đức',NULL,'Male','0854471145',N'Hà Tĩnh',1,NULL,NULL,2);

USE [OnlineLearn]
GO

INSERT INTO [dbo].[Setting] ([Name],[Mapped Value],[Type],[Order],[Status])
VALUES (N'Manager','','User Role',1,1),
(N'Teacher','','User Role',2,1),
(N'Student','','User Role',3,1);